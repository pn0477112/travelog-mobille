
import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class Phuot extends Component {
  
  render() {
    return (
    //1--------------------------------------------------
    <View style={styles.container}>
          {/* <View style={styles.productDetailContent}>
            <Text style={styles.productDetailName}> Mẹo Cực Hữu Ích Khi Phượt Xa</Text>
          </View> */}
          
          {/* <ScrollView style={styles.productDetail}>
          
          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../../image/anhPhuot/Hinh21.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>1.Lựa Chọn Lộ Trình</Text>
            <Text style={styles.productDetailInfo}>
            Hoạch định trước lộ trình của mình là một trong những việc quan trọng nhất trong các bước chuẩn bị “phượt”. Xác định điểm xuất phát và đích đến chỉ là công việc đơn giản, quan trọng nhất là bạn phải xác định những trạm nghỉ, dừng chân dọc đường. Ví dụ như bạn muốn đi “phượt” từ Sài Gòn đến Nha Trang, với chặng đường ngót nghét 600km, bạn nên chia ra thành nhiều chặng như Sài Gòn - Bình Thuận, Bình Thuận - Ninh Thuận, NInh Thuận - Nha Trang để có thơi gian nghỉ ngơi hồi sức, cũng như có thời gian đi khám phá các địa danh ở mỗi nơi.Ngoài việc lựa chọn những chặng dừng lớn cho lộ trình của mình, bạn cũng cần phải tìm hiểu trước những chặng dừng nhỏ trong khoảng 15-30 phút để có thời gian nghỉ ngơi cũng như vệ sinh cá nhân, biết đâu bạn có thể tìm thấy món gì đó nho nhỏ về làm quà cho người thân. Nên hỏi và tham khảo ý kiến của những người có kinh nghiệm hoặc nguồn uy tín để biết được những trạm dừng chân nào là “an toàn”, không sợ bị “chặt chém” hoặc vào nhầm các quán “cơm tù, phở tù” nhé.</Text>
          </View>
          

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../../image/anhPhuot/Hinh22.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>2.Những Vật Dụng Không Thể Thiếu</Text>
            <Text style={styles.productDetailInfo}>
            Áo khoác là một trong những vật không thể thiếu khi đi phượt, có thể là một chiếc áo khoác bằng vải dù đơn giản hoặc một chiếc áo khoác da cho đúng “chất”. Một chiếc áo khoác tốt sẽ giúp bạn giữ ấm cơ thể, cũng như cản bớt sức gió, giúp bạn ít mất sức hơn khi đi xa. Cùng với những lợi ích như áo khoác là nón bảo hiểm có kiếng, ngoài công dụng cản gió còn giúp bạn ngăn được bụi bẩn lọt vào mắt. Đừng coi thường nhé, khi chạy với tốc độ cao mà bị bụi bắn vào mắt là rất khó chịu đấy. Ngoài ra, khi đi các bạn nên hạn chế mặc quần ngắn, nên mặc quần dài và đi giày cao cổ, mang găng tay để không bị rát nắng (nhất là với các bạn nữ).Tiền bạc và giấy tờ là cực kỳ quan trọng ngay cả khi đi du lịch thông thường, với “phượt” thì càng quan trọng hơn gấp bội. Ngoài những khoản tiền “cứng” bắt buộc phải chi trả thì bạn cũng nên dự phòng kha khá, để lỡ có vấn đề gì cũng dễ xoay sở. Giấy tờ thì bạn cần chuẩn bị bằng lái xe, chứng minh thư, giấy đăng ký xe, bảo hiểm xe… Đây là những giấy tờ tùy thân nhất định phải mang theo. </Text>
          </View>

         
         
          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../../image/anhPhuot/Hinh23.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>3.Những Lỗi Vi Phạm Cần Tránh</Text>
            <Text style={styles.productDetailInfo}>
            Những lỗi quan trọng thường xảy ra nhất đối với dân “phượt” đó là chạy lấn sang làn đường khác và chạy vượt quá tốc độ. Với lỗi đầu tiên, các bạn cần chủ động sắp xếp đội hình các xe trong đội chạy theo một hàng, tránh tình trạng 2-3 xe chạy dàn hàng ngang, vừa cản đường, gây nguy hiểm cho người chạy sau, vừa có nguy cơ lấn sang làn đường bên cạnh rất lớn. Còn với lỗi thứ hai, bạn cần chú ý các biển báo tốc độ trên đường mình đi, và tuyệt đối không vì một phút hào hứng, bị mọi người kích động mà rồ ga, phóng nhanh vượt ẩu. Hãy luôn nhớ rằng, khi gây tai nạn, không chỉ một mình bạn mà người khác cũng gặp nguy hiểm. Ngoài ra, bị cảnh sát giao thông xử phạt hành chính tuy rằng có thể không nhiều, nhưng cũng làm bạn mất vui trong chuyến đi. Vì những điều trên, bạn hãy luôn tỉnh táo khi chạy xe.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../../image/anhPhuot/Hinh24.jpg")} style={styles.productDetailImg} />
          </View>

         

        </ScrollView> */}





        {/* <View style={styles.bottomOption}>
          <TouchableOpacity   onPress={() => {this.props.navigation.navigate('ChupAnh')}} 
          style={styles.cartTouch}>
            <Text style={styles.cartText}>Chuyển Qua Mẹo Khác</Text>
          </TouchableOpacity> */}
        {/* </View> */}
      </View>
    )
  }
}
// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   },
//   productDetail: {
//     marginTop: 5,
//   },
//   productDetailImg: {
//     resizeMode:'center',
//     width: '100%',
//    height:200
//   },
//   productDetailContent: {
//     marginVertical: 10,
//     paddingHorizontal: 10,
//     marginTop: 10,
//   },
//   productDetailName: {
//     fontSize: 25,
//     marginBottom: 10,
//     fontWeight: 'bold',
//   },
//   productDetailPrice: {
//     fontSize: 18,
//     color: 'red'
//   },
//   productDetailInfo: {
//     marginVertical: 10,
//     fontSize: 15
//   },
//   /* Cart */

//   bottomOption: {
//     height: 50,
//     flexDirection: 'row',
//     borderTopWidth: 1,
//     borderTopColor: '#eee'
//   },
//   cartTouch: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: '#05375a'
//   },
//   cartText: {
//     color: '#fff',
//   }

// });
