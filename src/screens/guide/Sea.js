
import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class Sea extends Component {

  render() {
    return (
    //1--------------------------------------------------
    <View style={styles.container}>
          <View style={styles.productDetailContent}>
            <Text style={styles.productDetailName}> Mẹo Vặt Khi Đi Du Lịch Biển</Text>
          </View>
          
          <ScrollView style={styles.productDetail}>
          
          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhSea/Hinh16.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>1.Khó chịu khi cát dính vào chân - phấn rôm là trợ thủ đắc lực</Text>
            <Text style={styles.productDetailInfo}>
            Chân dính cát khi đi chơi biển thực sự khiến bạn khó chịu bởi dù có phủi thế nào mà vẫn mãi không đi hết.Vậy thì bạn hãy mang theo lọ phấn rôm nhỏ theo mình nhé! Chỉ cần rắc chút phấn rôm lên phần chân bị dính cát, xoa đều là cát sẽ tự rơi ra khỏi bề mặt da. Đó là bởi phấn rôm có khả năng hút ẩm khá tốt, chúng sẽ tác động khiến phần cát nhanh khô, không còn bám dính bề mặt chân bạn nữa.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhSea/Hinh17.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>2.Nước vào tai, hãy thổi ngay 1 quả bóng</Text>
            <Text style={styles.productDetailInfo}>
            Khi nước vào tai mà làm mãi chúng vẫn không thoát ra được, bạn hãy lấy 1 quả bóng cao su và thổi chúng.Trong lúc vừa thổi đầy hơi vào bóng, bạn bịt mũi lại. Nhờ áp lực bên trong tai đẩy ra, nước trong tai sẽ tự động tràn ra ngoài.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhSea/Hinh18.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>3.Đi lặn biển tránh bị đau đầu - bịt mũi, ngậm miệng ngay nào</Text>
            <Text style={styles.productDetailInfo}>
            Do thay đổi áp lực khi lặn sâu dưới biển, những cơn đau đầu có thể ghé thăm bạn. Để tránh cuộc gặp gỡ không vui này, trước khi xuống nước, bạn hãy bịt mũi, ngậm miệng trong vòng 5 - 10 giây để tạo áp suất trong đầu.Sau đó, bạn thở sâu. Bạn cố gắng lặp lại động tác này khoảng 2 - 3 lần nhé!</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhSea/Hinh19.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>4.Để điện thoại không bị ngấm nước - túi zip xài liền tay</Text>
            <Text style={styles.productDetailInfo}>
            Một trong những nỗi lo đi biển đó là điện thoại bạn bị ngấm nước hay dính cát bẩn. Do đó, bạn hãy bỏ chiếc dế xịn vào túi zip và dùng ống hút hút sạch không khí bên trong ra. Giờ thì bạn có chiếc vỏ bao điện thoại "xịn sò" rồi đó!</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhSea/Hinh20.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>5.Chữa cháy da cấp tốc bằng lô hội</Text>
            <Text style={styles.productDetailInfo}>
            Nắng ở biển thường khá gay gắt và bạn sẽ khó tránh khỏi việc bị cháy da. Vì thế, lúc này bạn cần sự trợ giúp từ người bạn lô hội.Lô hội có chứa thành phần chữa vết thương, giảm sưng tấy, cấp ẩm nên sẽ là phương pháp hữu hiệu giúp chữa cháy nắng. Bạn có thể xoa trực tiếp lô hội lên da hoặc cho vào khay, để ngăn đá tủ lạnh. Khi cần, chà viên lô hội lên da cháy nắng nhé!</Text>
          </View>

        </ScrollView>





        <View style={styles.bottomOption}>
          <TouchableOpacity   onPress={() => {this.props.navigation.navigate('Phuot')}} 
          style={styles.cartTouch}>
            <Text style={styles.cartText}>Chuyển Qua Mẹo Khác</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  productDetail: {
    marginTop: 5,
  },
  productDetailImg: {
    
    width: '100%',
   height:750
  },
  productDetailContent: {
    marginVertical: 10,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  productDetailName: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  productDetailPrice: {
    fontSize: 18,
    color: 'red'
  },
  productDetailInfo: {
    marginVertical: 10,
    fontSize: 15
  },
  /* Cart */

  bottomOption: {
    height: 50,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  cartTouch: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#05375a'
  },
  cartText: {
    color: '#fff',
  }

});
