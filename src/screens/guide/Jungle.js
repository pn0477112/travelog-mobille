
import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class Jungle  extends Component {
  
  render() {
    return (
    //1--------------------------------------------------
    <View style={styles.container}>
          <View style={styles.productDetailContent}>
            <Text style={styles.productDetailName}>9 Bí Kíp Khi Bị Lạc Trong Rừng</Text>
          </View>
          
          <ScrollView style={styles.productDetail}>
          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh1.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>1.Hãy Cho Mọi Người Biết Khi Bạn Đi Ra Ngoài</Text>
            <Text style={styles.productDetailInfo}>
            Nếu có điều gì xảy ra với bạn trong rừng, cơ hội lớn nhất để bạn sống sót là được người khác cứu. Bằng cách nói với người quan tâm đến bạn nơi chính xác mà bạn đang tới, họ có thể liên lạc với các cơ quan chức năng để bắt đầu công cuộc tìm kiếm nếu bạn không trở về</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh2.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>2.Hãy Mang Theo Những Vật Dụng Cá Nhân Cần Thiết</Text>
            <Text style={styles.productDetailInfo}>
           Theo Cục Bảo vệ Môi trường bang New York (Mỹ), bạn không nên đi vào rừng một mình mà không mang theo nước, thực phẩm, dao, bật lửa, đồng hồ, còi và đèn pin. Ngoài ra, một số món đồ khác cần có là quần áo khô, điện thoại di động đầy pin và pin dự phòng.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh3.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>3.Hãy Giữ Cho Bản Thân Bình Tĩnh</Text>
            <Text style={styles.productDetailInfo}>
            Robert Koester, một chuyên gia tìm kiếm và là tác giả của cuốn sách Hành vi người bị lạc, cho rằng khi nhận ra mình bị lạc, điều đầu tiên bạn nên làm là ngồi xuống. Những quyết định khi bạn lo lắng có thể là những sai lầm trầm trọng. Hầu hết mọi người không nghĩ rằng mình sẽ bị lạc trong rừng cho đến khi chuyện đó xảy ra. Hoảng sợ sẽ khiến bạn lãng phí thời gian và đưa ra những quyết định tồi tệ</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh4.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>4.Hãy Ngồi Xuống Nơi Yên Tĩnh Và Bắt Đầu Suy Nghĩ</Text>
            <Text style={styles.productDetailInfo}>
            Hãy tự hỏi bản thân những câu hỏi quan trọng như “Làm cách nào mà mình đến đây?” hay “Còn bao lâu nữa thì trời tối?”.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh5.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>5.Hãy Xác Định Vị Trí Mình Đang Ở</Text>
            <Text style={styles.productDetailInfo}>
            Khi bạn bình tĩnh và biết mình còn bao nhiêu thời gian trước khi mặt trời lặn, hãy thử xác định các mốc có thể giúp bạn biết mình đang ở đâu. Những hình ảnh đặc biệt như núi và cây cối hoặc âm thanh như tiếng xe và nước chảy có thể giúp bạn tìm đường trở về một cách an toàn. Nếu không có những dấu hiệu trên, hãy nhớ những kiến thức có thể giúp bạn xác định phương hướng như ở vùng ôn đới, rêu thường mọc ở phía bắc của cây và đá. Hoặc, mạng nhện thường nằm ở phía nam của cái cây, theo Popular Mechanics.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh6.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>6.Hãy Lập Kế Hoạch</Text>
            <Text style={styles.productDetailInfo}>
            Hãy xác định xem bạn có thể về nhà trước khi mặt trời lặn hay phải ở lại cho đến sáng. Nếu phải ở lại qua đêm, Cục Bảo tồn Môi trường bang New York (Mỹ) khuyên: “Gom củi vào ban ngày dễ hơn nhiều so với ban đêm”. Bên cạnh đó, hãy xác định lượng gỗ thực sự cần để giữ lửa cả đêm.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh7.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>7.Hãy Đi Tìm Nước</Text>
            <Text style={styles.productDetailInfo}>
            Nước là thứ quan trọng nhất, thậm chí hơn cả thức ăn. Theo Koester, bạn có thể sống đến 4 tuần mà không ăn. Vì vậy, đừng lãng phí năng lượng tìm kiếm thức ăn trừ khi nó ở ngay gần bạn. Trong khi đó, nước thường chảy xuống dốc. Do vậy, hãy đi theo hướng đó nếu bạn đi tìm nước. Nếu đi qua một vũng nước lớn, bạn có thể làm sạch nước trước khi uống.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh8.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>8.Hãy Tìm Hoặc Làm Nơi Ở Ẩn Trú</Text>
            <Text style={styles.productDetailInfo}>
            Khi qua đêm trong rừng, một trong những rủi ro lớn nhất mà bạn phải đối mặt là tình trạng hạ thân nhiệt. Cách tốt nhất để tránh tình trạng này là tìm nơi trú ẩn để bạn có thể giữ khô và ấm cơ thể. “Hãy tìm một vị trí tránh gió mưa. Hang động là nơi rất tốt. Ngoài ra, những chỗ cạnh tảng đá lớn, một cái cây chết hoặc cây lớn cũng khá lý tưởng. Nếu tìm ra vị trí thích hợp, hãy cố gắng ở yên đó. Người khác sẽ dễ tìm thấy bạn khi bạn ngồi yên một chỗ hơn là di chuyển</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhRung/Hinh9.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>9.Hãy Tìm Kiếm Sự Giúp Đỡ</Text>
            <Text style={styles.productDetailInfo}>
            Tạo tiếng ồn (như thổi còi, đập đá vào với nhau hoặc hét lên), tạo dấu hiệu dễ nhìn thấy ở trên cao như ánh sáng phản chiếu của gương hoặc khói</Text>
          </View>

        </ScrollView>





        <View style={styles.bottomOption}>
          <TouchableOpacity   onPress={() => {this.props.navigation.navigate('Jungle2')}} 
          style={styles.cartTouch}>
            <Text style={styles.cartText}>Chuyển Qua Mẹo Khác</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  productDetail: {
    marginTop: 5,
  },
  productDetailImg: {
    
    width: '100%',
    height:290
  },
  productDetailContent: {
    marginVertical: 10,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  productDetailName: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  productDetailPrice: {
    fontSize: 18,
    color: 'red'
  },
  productDetailInfo: {
    marginVertical: 10,
    fontSize: 15
  },
  /* Cart */

  bottomOption: {
    height: 50,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  cartTouch: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#05375a'
  },
  cartText: {
    color: '#fff',
  }

});
