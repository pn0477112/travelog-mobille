
import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class Jungle2 extends Component {
    
    render() {
        return (
            //2--------------------------------------------------
            <View style={styles.container}>
                <View style={styles.productDetailContent}>
                    <Text style={styles.productDetailName}>Cần Chuẩn Bị Gì Khi Đi Rừng ?</Text>
                </View>

                {/* <ScrollView style={styles.productDetail}>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh10.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>1.Tìm Hiểu Điểm Đến</Text>
                        <Text style={styles.productDetailInfo}>
                            Đế có chuyến du lịch rừng đúng nghĩa, bạn nên chọn một khu rừng nguyên sinh vì đảm bảo được vẻ đẹp hoang sơ và hệ động thực vật đa dạng. Nên tìm hiểu trước về vị trí, độ an toàn và điều kiện thời tiết của nơi được chọn, nhằm đề phòng rủi ro trong chuyến đi, bạn nên thông báo trước với người thân, bạn bè về địa điểm, kế hoạch và thời gian lưu trú tại đó.Khi chọn được khu rừng thích hợp, bạn nên chọn vị trí tập kết cắm trại gần nơi có sông, suối và cách khu dân cư gần nhất khoảng 15km. Đặc biệt, bạn nên thuê một người dân địa phương thông thạo địa hình cùng tham gia chuyến đi. Họ sẽ giúp bạn tránh bị lạc đường hoặc tìm được nhiều đường đi tắt, đồng thời có thể giao tiếp được khi gặp người dân tộc thiểu số.</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh11.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>2.Chuẩn bị sức khỏe</Text>
                        <Text style={styles.productDetailInfo}>
                            Sức khỏe là yếu tố rất quan trọng giúp cho chuyến du lịch thành công. Vì vậy, bạn hãy chuẩn bị cho mình một sức khỏe thật tốt và nên tập thể dục thường xuyên trước khi bắt đầu chuyến đi. Trong đó, leo núi cần sự dẻo dai và sức chịu đựng của đôi chân do đó bạn nên quan tâm tâm chăm sóc đôi chân thật cẩn thận, để hạn chế tình trạng căng cơ, chuột rút khi di chuyển..</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh12.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>3.Nên đi theo nhóm đông người</Text>
                        <Text style={styles.productDetailInfo}>
                            Các bạn nên đi thành nhóm để tránh bị lạc đường và dễ dàng hỗ trợ lẫn nhau khi cần thiết. Nếu di chuyển trong vùng cây rậm rạp, bạn nên đánh dấu đoạn đường vừa đi qua bằng các dấu hiệu đơn giản để tránh quay lại chỗ cũ. Nếu có chướng ngại vật trên đường đi thì người đi trước phải thông báo cho người đi sau.</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh13.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>4.Trang phục đi rừng, vật dụng cần thiết</Text>
                        <Text style={styles.productDetailInfo}>
                            Khi đi rừng bạn nên mặc quần áo rộng rãi, thoải mái. Nếu du lịch vào mùa hè nên chọn áo phông bên trong và áo khoác dài tay bên ngoài để không bị các loại dây gai rừng làm trầy xước, đồng thời tránh được cảm lạnh do vã mồ hôi hay gặp gió rừng. Bên cạnh đó, bạn cũng nên trang bị một chiếc balô chống thấm nước và có dây thắt ngang bụng để dễ di chuyển. Một đôi giày vải mềm, hay giày cao su có khả năng chống trượt, cùng một chiếc mũ tại bèo cũng rất cần thiết.Ngoài ra, các vật dụng không thể thiếu trong chuyến du lịch rừng mà bạn phải mang theo gồm: dao, rìu, lều trại, túi ngủ, đèn pin, pin tiểu, máy ảnh, bật lửa, áo mưa, tấm trải, xoong nồi để nấu ăn. Các vật dụng y tế, thực phẩm quan trọng: túi sơ cấp cứu và thuốc y tế, thuốc đuổi côn trùng, kem chống muỗi, vắt, nước uống, thuốc khử trùng nước, thực phẩm đóng hộp, mỳ gói.</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh14.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>5. Cách xử lý những tình huống nguy hiểm</Text>
                        <Text style={styles.productDetailInfo}>
                                Tai nạn
                                Du lịch rừng luôn tiềm ẩn nhiều tai nạn không thể lường trước được, đòi hỏi bạn và đồng đội phải bình tĩnh để tìm cách xử lý. Trong đó, trường hợp sa vào bẫy thú rừng, gặp thuốc nổ hay lâm tặc… thường là những mối đe dọa hàng đầu. Các bạn nên tránh xa lán, trại của lâm tặc và chỉ nói xã giao vài câu rồi đi nếu chạm mặt.
                                Mặt khác, khi cắm trại gần sông, suối nên cẩn thận việc tắm rửa, giặt giũ và không nên tìm đến đỉnh các ngọn thác. Nếu gặp trời mưa bạn phải nhanh chân di chuyển lên cao để đề phòng những cơn lũ xảy ra bất chợt 
                                Côn trùng cắn, đốt
                                Bị côn trùng đốt khi đi rừng là chuyện khá phổ biến, đó là lý do bạn nên mang theo thuốc chống côn trùng, kem chống muỗi,… trước khi vào rừng bạn hãy dùng bình xịt muỗi xịt quanh ống quần từ đầu gối trở xuống để tránh bị côn trùng đốt. Bôi cao nóng quanh tất để tránh bị vắt cắn. đây là loại côn trùng hút máu tương tự con đỉa. Nếu chẳng may bị cắn, bạn xé chút giấy thấm nước bọt đậy lên vết thương để cầm máu.Đặc biệt, trong rừng rất nhiều rắn, bạn nên đập dập tỏi hòa với nước rồi rắc đều xung quanh vị trí nghỉ ngơi. Nếu bị rắn cắn nên buộc ca rô theo hướng dẫn của y tế và xuống núi thật nhanh. Trong trường hợp khẩn cấp, bạn có thể rạch 1 đường nhỏ tại vết thương và hút nọc độc ra.
                                Bị lạc
                                Nếu không may bị lạc, bình tĩnh chính là điều quan trọng mà bạn cần phải nhớ để tránh bị mất phương hướng do hoảng sợ. Sau đó bạn nên tìm những đường mòn hay đi theo dòng nước chảy để trở về xuôi. Ngoài ra, bạn có thể đốt lửa tạo khói, hoặc phát ra âm thanh để mọi người dễ dàng tìm thấy,…
                                Nếu bị lạc trong khi không còn gì để ăn, uống, bạn có thể ăn trái cây rừng (những loại bạn chắc chắn là không có độc), uống nước sương hứng từ lá cây và tuyệt đối không ăn những loại nấm độc, có màu sắc sặc sỡ.</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require("../../../image/anhRung/Hinh15.jpg")} style={styles.productDetailImg} />
                    </View>
                    <View style={styles.productDetailContent}>
                        <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>6.Kinh nghiệm ăn, uống, ngủ trong rừng</Text>
                        <Text style={styles.productDetailInfo}>
                        Đảm bảo ăn chín, uống sôi để bảo vệ sức khỏe. Ăn sáng thật no để duy trì nguồn năng lượng cần thiết trong một ngày. Riêng bữa trưa có thể ăn lương khô, bánh trái để tiện cho việc di chuyển.
                        Bạn nên dùng nước khoáng mang theo, hay một số loại nước tăng lực và không nên uống nước sông, suối (vì chứa nhiều vi khuẩn). Dọc đường đi nên uống chè sâm (hoặc pha cùng nước khoáng) vừa giải khát vừa khỏe người.
                        Khi ngủ trong rừng, chỗ ngủ nên xen giữa các hàng cây để phòng cây đổ lên người khi có bão. Chọn vị trí bằng phẳng, thông thoáng để ngã lưng thay vì những chỗ có nhiều đá (đặc biệt là khu vực dưới chân núi) tránh trường hợp lỡ đá. Tốt nhất, bạn hãy nhóm lửa khi ngủ để sưởi ấm và xua thú dữ. Nếu ngủ võng phải tránh những chỗ có vật nhọn hay đá tảng bên dưới nhằm tránh võng bị đứt có thể nguy hiểm cho bản thân.</Text>
                    </View>

                   


                    

                </ScrollView> */}





                <View style={styles.bottomOption}>
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('Sea')}}style={styles.cartTouch}>
                        <Text style={styles.cartText}>Chuyển Qua Mẹo Khác</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    productDetail: {
        marginTop: 5,
    },
    productDetailImg: {
        resizeMode: 'center',
        width: '100%',
        height: 200
    },
    productDetailContent: {
        marginVertical: 10,
        paddingHorizontal: 10,
        marginTop: 10,
    },
    productDetailName: {
        fontSize: 25,
        marginBottom: 10,
        fontWeight: 'bold',
    },
    productDetailPrice: {
        fontSize: 18,
        color: 'red'
    },
    productDetailInfo: {
        marginVertical: 10,
        fontSize: 15
    },
    /* Cart */

    bottomOption: {
        height: 50,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: '#eee'
    },
    cartTouch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#05375a'
    },
    cartText: {
        color: '#fff',
    }

});
