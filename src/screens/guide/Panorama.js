import React from 'react';
import {View,Dimensions, Image, ActivityIndicator} from 'react-native';
import {Text} from 'native-base';
// import { PanoramaView } from "@lightbase/react-native-panorama-view";
import {WebView} from 'react-native-webview';
// import {Data} from '../../../data/Data'

export default class Panorama extends React.Component{

    static navigationOptions = ({navigation}) => {
        return{
             title: 'Notification',
             headerShown: false
        };
        }
        constructor(props) {
          super(props);
          this.state = {
              panorama:1,
          }
        }
        
    render(){

        const Data1 = this.props.route.params.Data1;
        let {panorama} = this.state
        return(
            <View style={{flex: 1}}>
                {/* <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Bạn chưa có thông báo!</Text>
                </View> */}
                {/* <PanoramaView
                style={{ flex: 1 }}
                dimensions={{
                height: Dimensions.get("window").height,
                width: Dimensions.get("window").width
                }}
    inputType="stereo"
    imageUrl="https://goo.gl/maps/AoyBJ3nanuBHRoDc8"/> */}
         {
             panorama?
            <WebView source={{uri: Data1.panorama}}/>
           :
            <View>
            <Text>Khong Co Panorama</Text>
            </View>
                        
         }  
            </View>
        )
    }
}