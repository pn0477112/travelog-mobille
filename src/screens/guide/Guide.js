
import React from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity
} from 'react-native'
import Swiper from 'react-native-swiper';
import ImageOverlay from 'react-native-image-overlay';
import { ScrollView } from 'react-native-gesture-handler';
import { Card, Title, Paragraph} from 'react-native-paper';
const { width } = Dimensions.get('window')


export default class Guide extends React.Component{
        constructor(props) {
          super(props);
        }
    render(){
        return(
            <View style={{flex: 1, backgroundColor: 'white', alignSelf: 'stretch',}}>
          {/* <Text style={styles.title}>Mẹo cho chuyến đi của bạn</Text> */}
          <ScrollView>
              {/* Thẻ mẹo du lịch */}
               <Card onPress={()=> this.props.navigation.navigate("GuideCategory")}>
                <Card.Content >
                  <Title>Mẹo du lịch</Title>
                  <Paragraph>Khám phá và chia sẻ kinh nghiệm</Paragraph>
                  <Card.Cover 
                  source={{ uri: 'https://image.freepik.com/free-photo/hand-man-pointing-map_23-2147654296.jpg' }}
                  style={{height: 200, width: '100%', alignSelf: 'center'}} />
                </Card.Content>
              </Card>

              {/* Thẻ địa điểm du lịch */}
              <Card onPress={()=> this.props.navigation.navigate("MapsCategory")}>
                <Card.Content>
                  <Title>Địa điểm du lịch</Title>
                  <Paragraph>Gợi ý những địa điểm du lịch nổi tiếng</Paragraph>
                  <Card.Cover source={{ uri: 'https://image.freepik.com/free-photo/global-positioning-system-destination-marker-perforated-paper_53876-31042.jpg' }} />
                </Card.Content>
              </Card>
              </ScrollView>
          </View>
  )
    }
}
const styles = {
    container: {
      flex: 1
    },
  
    wrapper: {
    },
    title:{
      fontSize: 18,
      marginBottom: 5,
      marginLeft: 5,
      marginTop: 5,
      color: '#57606f'
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'transparent',
      borderRadius: 60,
      marginTop: 5
    },
  
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
  
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
  
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
  
    text: {
      color: '#fff',
      fontSize: 28,
      fontWeight: 'bold'
    },
  
    image: {
      width,
      flex: 1
    }
  }