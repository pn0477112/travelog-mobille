import React, { Component } from 'react';
import {View, ScrollView, StyleSheet, Image, TouchableOpacity,backgroundColor, ViewBase } from 'react-native';
import getDirections from 'react-native-google-maps-directions';
import { SafeAreaView } from 'react-native-safe-area-context';
import { List, Text} from 'react-native-paper';
export default class FLDetailDL extends Component {
  render() {
   
    // dong nay sửa lại getparam còn lại thì code vẫn y như cũ sửa 1 dòng này thôi nha
    const handleGetDirections = () => {
      const data = {
         source:{
          latitude:[],
          longitude:[]
         },
          
        
        destination: {
          latitude: parseFloat(Data.latitude),
          longitude: parseFloat(Data.longitude),
        },
      }
  
      getDirections(data)
    };
    const Data = this.props.route.params.Data;
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
        <View style={styles.header}>
            <Image source={{uri: Data.photo}} style={styles.productDetailImg} />
        </View>
        <View style={styles.mid}>
        <Text style={styles.productDetailName}>{ Data.text}</Text>
            <TouchableOpacity>
            <Text style={styles.productDetailPrice}>{ Data.address}</Text>
            </TouchableOpacity>   
      </View>
        <ScrollView>
        <View style={styles.end}>
          <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>Mô Tả</Text>
            <Text style={styles.productDetailInfo}>
            { Data.Description}          
          </Text>
          <Text style={{ color: '#333', fontSize: 16, fontWeight: 'bold' }}>Lời Khuyên</Text>
          <Text style={styles.productDetailInfo}>
          { Data.Recommend}          
          </Text>
        </View>
        </ScrollView>

        {/* <View style={styles.bottomOption}> */}
        <List.AccordionGroup >
              <List.Accordion title="Bình luận" id="1"
              left={props => <List.Icon {...props} icon="comment" />}>

              </List.Accordion>
              <List.Accordion title="Hình ảnh" id="2"
              left={props => <List.Icon {...props} icon="folder-multiple-image" />}>
               
              </List.Accordion>
              <View>
                <List.Accordion title="Công cụ" id="3"
                left={props => <List.Icon {...props} icon="toolbox" />}>
                <List.Item title="Panorama"  
                  onPress={()=> {this.props.navigation.navigate("Panorama", {Data1: Data})}}
                  left={props => <List.Icon {...props} icon="panorama-horizontal" />}/>
                <List.Item title="Chỉ đường đi" 
                  onPress={handleGetDirections}
                  left={props => <List.Icon {...props} icon="directions-fork" />}/>
                </List.Accordion>
              </View>
            </List.AccordionGroup>
        {/* </View> */}
        </ScrollView>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
    header:{
      // flex:20/5
  
    },
    productDetailImg: {
      width: '100%',
      height:200,
      resizeMode: 'cover'
    },
    mid:{
      marginVertical: 10,
      paddingHorizontal: 10,
      // marginTop: 20,
      // flex:1/6,
 
    },
    productDetailName: {
      fontSize: 20,
      // marginBottom: 10,
      fontWeight: 'bold',
    },
    productDetailPrice: {
      fontSize: 16,
      color: 'blue'
    },
    end:{
      flex:4/6,
      marginVertical: 10,
      paddingHorizontal: 10,
      marginTop: 20,
      
    },
    productDetailInfo: {
      marginVertical: 10,
      fontSize: 15
    },
  
  /* Cart */

  bottomOption: {
    height: 50,
    flexDirection: 'row',
    borderTopWidth: 2,
    borderTopColor: '#eee'
    
  },
  cartTouch: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#05375a'
  },
  cartText: {
    color: '#fff',
  }

});


