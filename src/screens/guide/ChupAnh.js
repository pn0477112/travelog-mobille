import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class ChupAnh extends Component {
      // let tabBarVisible = true;
      // if (navigation.state.index > 0) {
      //   tabBarVisible = false;
      // }
  render(){
    return (
    //1--------------------------------------------------
    <View style={styles.container}>
                
          <View style={styles.productDetailContent}>
            <Text style={styles.productDetailName}> 5 Mẹo Chụp Ảnh Đẹp Hơn Bằng Điện Thoại</Text>
          </View>
          
          <ScrollView style={styles.productDetail}>
          
          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh26.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>1.Điều chỉnh lấy nét và phơi sáng</Text>
            <Text style={styles.productDetailInfo}>
            Camera smartphone hiện nay đã được cải tiến rất nhiều, và hầu hết các thiết bị cầm tay đều cho phép bạn điều chỉnh chế độ lấy nét và phơi sáng khi chụp ảnh. Nếu thiết bị tích hợp tính năng lấy nét bằng tay, thì cách sử dụng vô cùng đơn giản, chỉ cần chạm vào màn hình ở điểm muốn lấy nét là được.Với chế độ phơi sáng, hay nói dễ hiểu là lượng ánh sáng cho phép vào trong khung hình thì cách điều chỉnh cũng không hề phức tạp. Camera trên iPhone thiết lập mặc định chế độ này, chỉ cần chạm vào màn hình lấy nét, sau đó giữ và kéo để điều chỉnh phơi sáng. Tương tự với smartphone Android, chạm vào chế độ lấy nét, sau đó điều chỉnh phơi sáng thông qua thanh trượt bật lên ở phía bên cạnh hoặc phía trên của màn hình.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh27.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>2.Sử dụng chế độ HDR</Text>
            <Text style={styles.productDetailInfo}>
            HDR là viết tắt của High Dynamic Range (tạm dịch là dải tương phản động mở rộng). Trên các smartphone cao cấp, HDR đều nhằm mục đích cân bằng ánh sáng tại các vùng có độ sáng tối khác nhau, để chắc chắn rằng không một vùng nào trong tấm hình bị thiếu sáng hay thừa sáng.Chế độ này đặc biệt phù hợp với chụp phong cảnh và chân dung, nhất là khi có một phạm vi rộng giữa các vùng tối và sáng trên bức ảnh. Để chụp chế độ này cần phải giữ ổn định điện thoại không bị rung lắc và tránh chụp các đối tượng đang di chuyển.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh28.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>3.Sử dụng ánh sáng tự nhiên để chụp hình</Text>
            <Text style={styles.productDetailInfo}>
            Ánh sáng là một trong những yếu tố quan trọng nhất cho bất kỳ bức ảnh nào. Nếu ánh sáng tự nhiên đủ cho bức ảnh, thì bạn không cần phải bật chế độ đèn flash của điện thoại vì điều này sẽ có thể khiến bức ảnh kém tự nhiên hơn.Bạn nên định vị các đối tượng sao cho phía trước đủ sáng, và phía sau thì không bị rọi bóng do ánh sáng quá mạnh. Tất nhiên, trong trường hợp cần phải chụp nhanh chóng thì khó có thể chờ để chọn điều kiện sáng tốt nhất.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh29.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>4.Giữ chắc thiết bị tránh rung nhòe</Text>
            <Text style={styles.productDetailInfo}>
            Một nhược điểm khi chụp ảnh bằng smartphone so với máy ảnh chuyên dụng chính là đảm bảo độ ổn định thiết bị. Ống kính của điện thoại chụp ảnh rất nhỏ nên rất nhạy cảm với những động tác rung tay. Chỉ cần một chuyển động nhẹ cũng có thể khiến cho bức ảnh bị nhòe mờ.Do đó điều quan trọng là giảm thiểu rung máy càng nhiều càng tốt. Thậm chí, bạn có thể chọn mua giá đỡ 3 chân giá rẻ sản xuất riêng cho smartphone để sử dụng. Còn nếu không, có thể tận dụng một vật cố định như bức tường, valy khi du lịch hoặc thậm chí là cánh tay còn lại của bạn. Giữ cho máy ảnh ổn định còn đặc biệt quan trọng trong điều kiện ánh sáng yếu, khi mà tốc độ chớp sáng của điện thoại cao hơn.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh30.jpg")} style={styles.productDetailImg} />
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>5.Áp dụng quy tắc 1/3 cho ảnh đẹp hơn</Text>
            <Text style={styles.productDetailInfo}>
            Muốn bức ảnh có sức cuốn hút và chiều sâu, bạn nên dịch chuyển chủ thể sang bên một cách tinh tế. Khung hình được chia làm 9 phần đều nhau bởi 2 đường ngang và 2 cột dọc. Sau đó đặt chủ thể hoặc điểm cần nhấn vào một trong 4 giao điểm của các đường này, cách đó sẽ dễ dàng hướng ánh mắt người xem vào chủ đề và tạo một bố cục cân đối hơn.Có một số nguyên tắc mà các bạn cũng nên chú ý như: đường chân trời ở 1/3 hoặc 2/3 chiều cao bức ảnh, điểm nhấn của ảnh đặt toạ độ 1/3 rộng x 1/3 cao hay tận dụng nét lượn chữ S nếu có trong bối cảnh.Ứng dụng camera trên điện thoại thường sẽ tích hợp sẵn khung hình dạng này. Trên thiết bị Android, bạn có thể thấy biểu tượng Lưới ở cuối màn hình, còn với thiết bị iOS, có thể vào Cài đặt, chọn Ảnh&Camera rồi kích hoạt tính năng Lưới.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <Image source={require("../../image/anhChupAnh/Hinh31.jpg")} style={styles.productDetailImg} />
          </View>
          
        </ScrollView>

        {/* <View style={styles.bottomOption}>
        </View> */}
      </View>
    )
}
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  productDetail: {
    marginTop: 5,
  },
  productDetailImg: {
   
    width: '100%',
   height:300
  },
  productDetailContent: {
    marginVertical: 10,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  productDetailName: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  productDetailPrice: {
    fontSize: 18,
    color: 'red'
  },
  productDetailInfo: {
    marginVertical: 10,
    fontSize: 15
  },
  /* Cart */

  bottomOption: {
    height: 50,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  cartTouch: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#05375a'
  },
  cartText: {
    color: '#fff',
  }

});