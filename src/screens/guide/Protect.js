
import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
export default class Protect extends Component {
  render() {
    return (
    //1--------------------------------------------------
    <View style={styles.container}>
          <View style={styles.productDetailContent}>
            <Text style={styles.productDetailName}> Nữ giới đi phượt một mình cần lưu ý gì?</Text>
          </View>
          
          <ScrollView style={styles.productDetail}>
          
          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh32.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>1.Những lưu ý bắt buộc</Text>
            <Text style={styles.productDetailInfo}>
            Scan toàn bộ giấy phép lái xe, passport, giấy khám sức khoẻ và các giấy tờ quan trọng, sau đó gửi mail cho chính mình và cho bạn bè hoặc người thân. Nếu lỡ mất giấy tờ hoặc có bất trắc xảy ra, ít nhất bạn cũng có thể tìm thấy bản sao trong email.Luôn nói cho một người khác biết bạn sắp đi đến đâu, khi nào sẽ đến và ở đâu. Khi đến nơi, hãy gọi điện xác nhận lại những thông tin này. Cách này sẽ giúp người thân hoặc bạn bè luôn biết bạn đang ở đâu để ứng cứu khi cần thiết.Hãy tin vào trực giác. Nếu bị ai đó tiếp cận và cảm thấy không thoải mái, hãy thẳng thắn và đừng sợ làm mất lòng người khác. Khi đi xa, hãy đặt vấn đề an toàn của bản thân lên hàng đầu.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh33.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>2.Cẩn thận khi di chuyển</Text>
            <Text style={styles.productDetailInfo}>
            Đối với phương tiện công cộng, điều bạn luôn phải lưu ý là đề phòng bị móc túi. Đừng mang ba lô phía sau lưng và hãy luôn để ý đến những người xung quanh.Đừng nghĩ là chỉ có nam giới mới đi móc túi. Đôi lúc, có những nhóm phụ nữ giả vờ tông vào bạn hoặc vây quanh bạn trên xe buýt. Lúc họ xuống xe cũng là lúc bạn phát hiện tài sản của mình không cánh mà bay.Trên các chuyến xe đường dài, nếu muốn ngồi gần một người phụ nữ, hãy mạnh dạn lên tiếng. Nếu không rõ lộ trình, hãy nói với bác tài xế và nhờ họ thông báo khi gần đến nơi dừng.Ngoài ra, hãy tìm thông tin về những hãng taxi đáng tin cậy trong khu vực và giá cả mỗi hãng. Bạn có thể hỏi các nhân viên ở sân bay, trạm xe buýt hoặc tại khách sạn. Đừng leo lên xe rồi mới hối hận vì mình bị lừa hoặc đi nhầm các hãng xe “dù”.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh34.png")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>3.Hãy sống như người bản địa</Text>
            <Text style={styles.productDetailInfo}>
            Nếu có thể, hãy cố gắng hoà nhập càng nhanh càng tốt. Đừng tỏ ra ngơ ngác và để bất cứ ai nhìn vào cũng thấy bạn là vị khách từ phương xa đến. Hãy tìm hiểu kỹ về nơi mình sắp đến, đặc biệt là trang phục hoặc tập tục truyền thống và cách hành xử phù hợp.Đừng sử dụng túi xách hoặc ba lô quá bắt mắt trừ phi bạn đủ khả năng chống trả nếu bị cướp giật. Bạn có thể dùng chiếc túi cũ một chút, không có thương hiệu nổi bật, nhưng đủ bền để chứa máy ảnh, laptop, máy nghe nhạc và hàng tá đồ điện tử khác. Đừng để cho bọn cướp thấy rằng trong túi của bạn “hẳn phải có nhiều thứ đắt tiền”.Đừng để cho ai thấy các hoá đơn mua hàng có giá trị quá lớn. Nếu cần phải xem bản đồ, đừng đứng ngay giữa đường mà hãy vào một cửa hàng nào đó.Nếu đi một mình, bạn hãy tránh vừa đi vừa nghe nhạc. Thói quen này nghe có vẻ thi vị, nhưng nó khiến bạn mất khả năng tập trung và chú ý đến những rủi ro xung quanh.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh35.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>4.Đừng tìm cách “phá luật”</Text>
            <Text style={styles.productDetailInfo}>
            Nếu đã quyết định đi du lịch một mình, bạn hãy đảm bảo mình đủ khôn khéo để tự bảo vệ bản thân khỏi nguy hiểm. Đừng đi bộ quá xa hoặc sử dụng phương tiện giao thông công cộng khi trời đã tối, đặc biệt nếu bạn cảm thấy mình có thể bị lạc.Đừng bao giờ uống rượu bia tới mức say xỉn. Đây là một trong những điều tuyệt đối cấm kỵ đối với một nữ du khách đi một mình, vì bạn rất dễ trở thành mục tiêu cho những kẻ xấu.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh36.jpg")} style={styles.productDetailImg} /> */}
          </View>
          <View style={styles.productDetailContent}>
            <Text style={{ color: '#333', fontSize: 20, fontWeight: 'bold' }}>5.Hãy thân thiện và tươi cười</Text>
            <Text style={styles.productDetailInfo}>
            Hãy luôn tươi cười. Khi bạn tỏ ra thân thiện, người khác sẽ dễ có cảm tình và muốn giúp đỡ bạn hơn.
            Bạn có thể xin bản đồ ở khách sạn và nhờ nhân viên hướng dẫn những địa điểm nên hoặc không nên đến. Ngoài ra, hãy hỏi thêm về những rủi ro mà bạn có thể gặp phải tại khu vực mình đang ở.
            Mỗi khi đi ra ngoài, hãy luôn mang theo danh thiếp có kèm tên và địa chỉ của khách sạn để trong trường hợp lạc đường hoặc say xỉn, bạn còn biết chỗ mà quay về.Nếu bị rơi vào tình huống bị ai đó “xin đểu”, hãy khéo léo tìm cách thoát thân, nhờ người giải cứu hoặc tệ nhất là đưa cho họ thứ họ muốn. Đừng gây rối thêm hoặc tìm cách đánh trả.</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            {/* <Image source={require("../../../image/anhProtect/Hinh37.jpg")} style={styles.productDetailImg} /> */}
          </View>
          
        </ScrollView>





        <View style={styles.bottomOption}>
          <TouchableOpacity   onPress={() => {this.props.navigation.navigate('Jungle')}} 
          style={styles.cartTouch}>
            <Text style={styles.cartText}>Chuyển Qua Mẹo Khác</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  productDetail: {
    marginTop: 5,
  },
  productDetailImg: {
   
    width: '100%',
   height:250
  },
  productDetailContent: {
    marginVertical: 10,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  productDetailName: {
    fontSize: 25,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  productDetailPrice: {
    fontSize: 18,
    color: 'red'
  },
  productDetailInfo: {
    marginVertical: 10,
    fontSize: 15
  },
  /* Cart */

  bottomOption: {
    height: 50,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  cartTouch: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#05375a'
  },
  cartText: {
    color: '#fff',
  }

});
