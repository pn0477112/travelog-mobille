import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList,TextInput,Keyboard,Image,TouchableOpacity } from 'react-native';
import { SearchableFlatList } from "react-native-searchable-list";
import { Searchbar } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
export default class FLSapa extends Component {

constructor(props) {
  super(props);
  this.state = {
    // data: [SAIGON],
    searchTerm: "",
    searchAttribute: "text",
    ignoreCase: false,
    listProduct: []
  };
}
componentDidMount() {
fetch('http://172.20.10.4:80/sapa.php')
 .then((response) => response.json())
 .then((responseJson) => {
 this.setState({
 listProduct: responseJson,
 });
 });

 }
render() {
  const { data, searchTerm, searchAttribute, ignoreCase } = this.state;
  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={styles.pageContainer}>
          <View style={styles.searchInputs}>
            <Searchbar
              style={styles.search}
              placeholder={
                ignoreCase
                  ? "Search Wonder Country"
                  : "Tìm kiếm"
              }
              onChangeText={searchTerm => this.setState({ searchTerm })}
            />
          </View>
          <SearchableFlatList
            style={styles.listItem}
            data={this.state.listProduct}
            searchTerm={searchTerm}
            searchAttribute={searchAttribute}
            ignoreCase={ignoreCase}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('FLDetailSP', { Data: item}) }}>
              <View style={styles.item}>
                  <Image source={{uri:item.photo}}  style={styles.img}/>
                  <Text numberOfLines={8} ellipsizeMode='head' multiline={true} style={styles.text}> {item.text} </Text>
              </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        </View>
        </View> 
  );
}
}



const styles = StyleSheet.create({
pageContainer: {
  padding: 10,
  flex: 1,
  marginTop:10
},
searchInputs: {
  flexDirection: "row",
  height: 40,
},
search: {
  flex: 8,
  // borderColor: "#05375a",
  // borderBottomWidth: 3,
  marginTop: -15,
  marginBottom: -10,
  padding: 10,
  fontSize:16,
  borderRadius: 10,
},
switch: {
  flex: 2
},
listItem: {
  padding: 10,
  // borderColor: "#05375a",
  // borderWidth: 1,
  borderRadius: 10,
  margin: 2
},
info: {
  padding: 10,
  marginTop: 20,
  borderColor: "#f4cfce",
  borderWidth: 1
},

item: {
  width: '95%',
  flexDirection: 'row',
  // borderBottomWidth: 1,
  // borderBottomColor: 'black',
  alignItems: 'center',
  marginVertical:5,
 
  
},
text: {
  // marginVertical: 30,
  fontSize: 17.5,
  fontWeight: 'bold',
  marginLeft: 8,
  marginRight: 8,
}, 
img:{
  height:100, width:100, borderColor: 'gray', borderRadius: 10
}
});