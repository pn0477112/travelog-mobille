
import React,{useState} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity
} from 'react-native'
import Swiper from 'react-native-swiper';
import ImageOverlay from 'react-native-image-overlay';
import { ScrollView } from 'react-native-gesture-handler';
import { List, Card } from 'react-native-paper';
const { width } = Dimensions.get('window')

export default class GuideCategory extends React.Component{
    
        constructor(props) {
          super(props);
        }
    render(){
        return(
            <View style={{flex: 1, backgroundColor: 'white', alignSelf: 'stretch',}}>
          {/* <Text style={styles.title}>Mẹo cho chuyến đi của bạn</Text> */}
          <View style={{height:'50%', width: "100%"}}>
          <Swiper style={styles.wrapper} height={200} horizontal={true} autoplay userNativeDriver={true}>
            <View style={styles.slide2}>
              <TouchableOpacity style={styles.slide1} onPress={() => this.props.navigation.navigate('Jungle')}>
                <Image style={styles.image} source={{uri: 'https://image.freepik.com/free-vector/user-looking-information-tablet-with-magnifier-illustration_335657-322.jpg' }} />
              </TouchableOpacity>
              </View>
          
              <View style={styles.slide2}>
              <TouchableOpacity style={styles.slide1} onPress={() => this.props.navigation.navigate('Sea')}> 
                <Image style={styles.image} source={{uri: 'https://image.freepik.com/free-vector/illustration-characters-with-traveling-concept_53876-40812.jpg' }} />
              </TouchableOpacity>
            </View>
          </Swiper>
          </View>
          <List.AccordionGroup>
                <List.Accordion title="Đời sống" id="1">
                <List.Item title="Nhiếp ảnh"
                     onPress={()=> this.props.navigation.navigate("ChupAnh")}
                    description="Những mẹo chụp ảnh hay"
                    left={props => <List.Icon {...props} icon="camera" />}/>
                <List.Item title="Sự kiện"
                    onPress={()=> this.props.navigation.navigate("Jungle2")}
                    description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="calendar-edit" />}/>
                </List.Accordion>
                <List.Accordion title="Du lịch & trải nghiệm" id="2">
                    <List.Item title="Đi rừng"
                        onPress={()=> this.props.navigation.navigate("Jungle")}
                        description="Những mẹo cần biết khi đi rừng"
                        left={props => <List.Icon {...props} icon="pine-tree" />}/>
                    <List.Item title="Đi phượt"
                        onPress={()=> this.props.navigation.navigate("Phuot")}
                        description="Những mẹo cần biết khi đi phượt"
                        left={props => <List.Icon {...props} icon="motorbike" />}/>
                    <List.Item title="Đi biển"
                        onPress={()=> this.props.navigation.navigate("Sea")}
                        description="Những mẹo cần biết khi đi biển"
                        left={props => <List.Icon {...props} icon="waves" />}/>
                </List.Accordion>
                <List.Accordion title="Dành cho bạn" id="3">
                    <List.Item title="Bảo vệ bản thân"
                        onPress={()=> this.props.navigation.navigate("Protect")}
                        description="Những mẹo để bảo vệ bản thân khi đi du lịch"
                        left={props => <List.Icon {...props} icon="shield-sun" />}/>
                    <List.Item title="Gợi ý từ Travelog"
                        onPress={()=> this.props.navigation.navigate("Protect")}
                        description="Mẹo du lịch gợi ý bởi chính chúng tôi"
                        left={props => <List.Icon {...props} icon="lightbulb-on" />}/>
                </List.Accordion>
            </List.AccordionGroup>
          </View>
  )
    }
}
const styles = {
    container: {
      flex: 1
    },
  
    wrapper: {
    },
    title:{
      fontSize: 18,
      marginBottom: 5,
      marginLeft: 5,
      marginTop: 5,
      color: '#57606f'
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'transparent',
      borderRadius: 60,
      marginTop: 5
    },
  
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
  
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
  
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
  
    text: {
      color: '#fff',
      fontSize: 28,
      fontWeight: 'bold'
    },
  
    image: {
      width,
      flex: 1
    },
    wrapper: {
      justifyContent: 'center'
    },
    title:{
      fontSize: 18,
      marginBottom: 5,
      marginLeft: 5,
      marginTop: 5,
      color: '#57606f'
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'transparent',
      borderRadius: 60,
      marginTop: 5
    },
  
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },

  }