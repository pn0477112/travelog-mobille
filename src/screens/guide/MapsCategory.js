
import React,{useState} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity
} from 'react-native'
import Swiper from 'react-native-swiper';
import ImageOverlay from 'react-native-image-overlay';
import { ScrollView } from 'react-native-gesture-handler';
import { List, Colors ,Card} from 'react-native-paper';
import BannerComponet from '../../components/Banner';
const { width } = Dimensions.get('window')

export default class MapsCategory extends React.Component{

    render(){
        return(
            <View style={{flex: 1, backgroundColor: 'white', alignSelf: 'stretch',}}>
          {/* <Text style={styles.title}>Mẹo cho chuyến đi của bạn</Text> */}
          {/* <BannerComponet/> */}
          <View style={{height:'35%', width: "100%"}}>
          <Swiper style={styles.wrapper} height={200} horizontal={true} autoplay userNativeDriver={true}>
            <View style={styles.slide2}>
              <TouchableOpacity style={styles.slide1} onPress={() => this.props.navigation.navigate('Jungle')}>
                <Image style={styles.image} source={{uri: 'https://image.freepik.com/free-vector/travel-vietnam-concept_98402-1479.jpg' }} />
              </TouchableOpacity>
              </View>
          
              <View style={styles.slide2}>
              <TouchableOpacity style={styles.slide1} onPress={() => this.props.navigation.navigate('Sea')}> 
                <Image style={styles.image} source={{uri: 'https://image.freepik.com/free-photo/smiling-young-man-holding-map-showing-thumb-up-gesture-outdoors_23-2148203031.jpg' }} />
              </TouchableOpacity>
            </View>
          </Swiper>
          </View>
          <List.AccordionGroup>
                    <List.Item title="Địa điểm du lịch Sài Gòn"
                        onPress={()=> this.props.navigation.navigate("FLSaiGon")}
                        description="Địa điểm nổi bật"
                        left={props => <List.Icon {...props} icon="source-commit-end-local" />}/>
                    <List.Item title="Địa điểm du lịch Đà Lạt"
                        onPress={()=> this.props.navigation.navigate("FLDaLat")}
                        description="Địa điểm nổi bật"
                        left={props => <List.Icon {...props} icon="source-commit-local" />}/>
                    <List.Item title="Địa điểm du lịch Hà Nội"
                        onPress={()=> this.props.navigation.navigate("FLHaNoi")}
                        description="Địa điểm nổi bật"
                        left={props => <List.Icon {...props} icon="source-commit-local" />}/>
                    <List.Item title="Địa điểm du lịch Sapa"
                        onPress={()=> this.props.navigation.navigate("FLSapa")}
                        description="Địa điểm nổi bật"
                        left={props => <List.Icon {...props} icon="source-commit-next-local" />}/>
                    <List.Item title="Địa điểm du lịch Vũng Tàu"
                        onPress={()=> this.props.navigation.navigate("FLVungTau")}
                        description="Địa điểm nổi bật"
                        left={props => <List.Icon {...props} icon="source-commit-start-next-local" />}/>
            </List.AccordionGroup>
          </View>
  )
    }
}
const styles = {
    container: {
      flex: 1
    },
  
    wrapper: {
    },
    title:{
      fontSize: 18,
      marginBottom: 5,
      marginLeft: 5,
      marginTop: 5,
      color: '#57606f'
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'transparent',
      borderRadius: 60,
      marginTop: 5
    },
  
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
  
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
  
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
  
    text: {
      color: '#fff',
      fontSize: 28,
      fontWeight: 'bold'
    },
  
    image: {
      width,
      flex: 1
    }
  }