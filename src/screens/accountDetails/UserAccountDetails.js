// main screen of user account

import React, { useContext, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Button,
} from "react-native";

// firebase
import firebase from "firebase/app";

import { globalStyles } from "../../globalStyles";
import { UserContext } from "../../context";
import ShowFriendsRequest from "./ShowFriendsRequest";
import AppButton from "../../components/AppButton";
import ShowPost from "./ShowPost";
import { List, Colors ,Card} from 'react-native-paper';
const UserAccountDetails = ({ navigation }) => {
  const { uid, user, setUser } = useContext(UserContext);

  const getUser = async () => {
    await firebase
      .database()
      .ref("/usersPublicDetails/" + uid)
      .on("value", (snapshot) => {
        setUser(snapshot.val());
      });
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'white', justifyContent: 'center', alignContent: 'center'}}>
      <ScrollView>
        <Text style={globalStyles.titleText}>Trang cá nhân</Text>

        {user ? (
          <>
            <View style={{justifyContent: 'center', alignSelf: 'center'}}>
              <Image
                source={{ uri: user.profileImageUrl }}
                style={styles.image}
              />
              <Text style={{justifyContent: 'center', alignSelf: 'center'}}>{user.name}</Text>
            </View>
          </>
        ) : null}

        <View>
          <ShowFriendsRequest />
        </View>
       <List.AccordionGroup>
            <List.Accordion title="Người dùng" id="1">
                <List.Item title="Xem  thông tin"
                     onPress={()=> navigation.navigate("EditUserDetails")}
                    // description="Những mẹo chụp ảnh hay"
                    left={props => <List.Icon {...props} icon="information" />}/>
                <List.Item title="Đăng bài viết"
                    onPress={()=> navigation.navigate("AddPost")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="plus-circle" />}/>
                <List.Item title="Thêm bạn bè"
                    onPress={()=> navigation.navigate("AddFriends")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="account-multiple-plus" />}/>
                <List.Item title="Danh sách bạn bè"
                    onPress={()=> navigation.navigate("FriendsList")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="account-details" />}/>
                <List.Item title="Kênh chat chung"
                    // onPress={()=> this.props.navigation.navigate("Jungle2")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="chat-processing" />}/>
                </List.Accordion>

                <List.Accordion title="Cài đặt tài khoản" id="2">
                <List.Item title="Thay đổi mật khẩu"
                    onPress={()=> navigation.navigate("ChangePassword")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="lock-check" />}/>
                <List.Item title="Thay đổi email"
                    // onPress={()=> this.props.navigation.navigate("Jungle2")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="email-check" />}/>
                </List.Accordion>

                <List.Accordion title="Cài đặt ứng dụng" id="3">
                <List.Item title="Thông tin ứng dụng"
                    onPress={()=> navigation.navigate("ForgetPass")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="lock-alert" />}/>
                <List.Item title="Phản hồi"
                    // onPress={()=> this.props.navigation.navigate("Jungle2")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="message-alert" />}/>
                <List.Item title="Điều khoản"
                    // onPress={()=> this.props.navigation.navigate("Jungle2")}
                    // description="Những sự kiện du lịch nổi bật"
                    left={props => <List.Icon {...props} icon="book-lock" />}/>
                </List.Accordion>
            </List.AccordionGroup>

        <View>
          <ShowPost />
        </View>
      </ScrollView>
    </View>
  );
};

export default UserAccountDetails;

const styles = StyleSheet.create({
  image: {
    height: 120,
    width: 120,
    borderRadius: 60
  },
  buttonContainer: {
    flexDirection: "row",
    height: 50,
    marginVertical: 5,
    alignContent: "center",
    justifyContent: "space-evenly",
  },
});
