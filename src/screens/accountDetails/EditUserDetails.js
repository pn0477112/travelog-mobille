import React, { useState, useContext } from "react";
import { StyleSheet, Text, View, TextInput, ScrollView } from "react-native";

// firebase
import firebase from "firebase/app";

import { globalStyles } from "../../globalStyles";
import { UserContext } from "../../context";

import ImageInput from "../../components/ImageInput";
import AppButton from "../../components/AppButton";

const EditUserDetails = ({ navigation }) => {
  const { uid, user } = useContext(UserContext);

  const [name, setName] = useState(null);
  const [imageUri, setImageUri] = useState(null);

  // function  to update user details to database
  const updateUserDetails = async () => {
    const response = await fetch(imageUri);
    const blob = await response.blob();
    await firebase
      .storage()
      .ref(uid)
      .child("profileImage")
      .put(blob)
      .then(() => {
        firebase
          .storage()
          .ref(uid)
          .child("profileImage")
          .getDownloadURL()
          .then((profileImageUrl) => {
            firebase
              .database()
              .ref("/usersPublicDetails/" + uid)
              .update({
                name,
                profileImageUrl,
              })
              .then((res) => {
                alert("Details Update");
                navigation.navigate("UserAccountDetails");
              })
              .catch((error) => {
                alert(error);
              });
          });
      });
  };

  return (
    <ScrollView style={globalStyles.container}>
      <Text style={globalStyles.titleText}>Chỉnh sửa thông tin cá nhân</Text>
      {user ? (
        <Text>Nhập đầy đủ thông tin.</Text>
      ) : (
        <Text> Nhập đầy đủ thông tin nếu bạn cập nhật lần đầu tiên.</Text>
      )}

      <ImageInput imageUri={imageUri} setImageUri={setImageUri} />

      <TextInput
        keyboardType="default"
        style={globalStyles.input}
        placeholder="Nhập tên"
        onChangeText={(val) => setName(val)}
        value={name}
      />

      <AppButton title="Post" onPress={updateUserDetails} />
    </ScrollView>
  );
};

export default EditUserDetails;

const styles = StyleSheet.create({});
