import React, { useState, useContext } from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";

// Firebase
import firebase from "firebase/app";

// Usrr Context
import { UserContext } from "../../context";

// globalStyles
import { globalStyles } from "../../globalStyles";
import AppButton from "../../components/AppButton";

const ChangePassword = ({ navigation }) => {
  // const { uid, setUid } = useContext(UserContext);
  // const [email, setEmail] = useState(null);
  // const [password, setPassword] = useState('');
  // const [currentPassword, setCurrentPassword] = useState('');
  // const reauthenticate = (currentPassword) => {
  //   var user = firebase.auth().currentUser;
  //   var cred = firebase.auth.EmailAuthProvider.credential(email, currentPassword);
  //   return user.reauthenticateWithCredential(cred);

  // }
  // onChangePasswordPress = () => {
  //   this.reauthenticate(currentPassword).then(() => {
  //     var user = firebase.auth().currentUser;
  //     user.updatePassword(password).then(() => {
  //       Alert.alert("Password was changed");
  //     }).catch((error) => { console.log(error.message); });
  //   }).catch((error) => { console.log(error.message) });
  // }
  // const handleSignUp = () => {
  //   firebase
  //     .auth()
  //     .createUserWithEmailAndPassword(email, password)
  //     .then((res) => {
  //       setUid(res.user.uid);
  //       alert("Sign Up");
  //       //navigation.navigate("AccounStack");
  //     })
  //     .catch((error) => {
  //       alert(error);
  //     });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();
    // reauthenticate();
    // onChangePasswordPress();
    // handleSignUp();
  };

  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.titleText}>Đổi mật khẩu</Text>

      <TextInput
        keyboardType="email-address"
        style={globalStyles.input}
        placeholder="Nhập mật khẩu"
        onChangeText={(val) => setPassword(val)}
        value={password}
      />
      {/* <TextInput
        secureTextEntry
        style={globalStyles.input}
        placeholder="Nhập Password"
        onChangeText={(val) => setPassword(val)}
        value={password}
      /> */}

      <AppButton title="Đổi mật khẩu" onPress={handleSubmit} />
    </View>
  );
};

export default ChangePassword;
