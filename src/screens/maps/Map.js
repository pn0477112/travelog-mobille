import React from 'react';
import { TextInput,Text, View, ScrollView, StyleSheet, Image, TouchableOpacity,backgroundColor,Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker,Polyline,Callout } from 'react-native-maps';
import MapViewDirections from "react-native-maps-directions";
import {IMAGE} from '../../Image';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PlacesInput from 'react-native-places-input';
export default class Map extends React.Component{

      constructor(props) {
          super(props);
          // Khai báo listProduct rỗng
          this.state = {
          listProduct: []
          };
          }
          //Lấy data từ phpadmin
          componentDidMount() {
              fetch('http://192.168.1.13:80/saigon.php')
               .then((response) => response.json())
               .then((responseJson) => {
               this.setState({
               listProduct: responseJson,
               });
               });
               }
        
        render() {
          //hàm bắt sự kiện tìm kiếm
          const _handlePlace = (place) =>{
            console.log(place)
            let value = {
              latitude: parseFloat(place.result.geometry.location.lat),
              longitude: parseFloat(place.result.geometry.location.lng),
              latitudeDelta: 0.04864195044303443,
              longitudeDelta: 0.04864195044303443,
            };
              this.map.animateToRegion(value, 350);
          }
          const GOOGLE_API_KEY = 'AIzaSyA4GskVuacaoKJTKN6LFjgRwLk0XZ0hd28'
          const GOOGLE_API_KEY_PLACE = 'AIzaSyA0yoMZCwV7mVB5A6noR4KEWVU5yJThu2Y';
          const origin = { latitude:10.775676,longitude:106.700226 };
          const destination = { latitude:10.788247, longitude:106.704705 };
          
        
          
          return (
            <View  style={{ flex: 1 }}>
           {/* <CustomHeader title="Nhật ký tổng hợp" isHome={true} navigation={this.props.navigation}/> */}
            <MapView
              ref={map => this.map = map}
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={{ flex: 1 }}
              showsUserLocation={true}
              region={{
                latitude: 10.775676,
                longitude: 106.700226, 
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            >
              
              {this.state.listProduct.map(listProduct => (
              <Marker
                coordinate={{latitude:  parseFloat(listProduct.latitude), 
                  longitude:parseFloat(listProduct.longitude)}}
                anchor={{ x: 0.5, y: 0.5 }}
                flat={false}
                title="Chuyến xe Vip"
                description="Địa điểm Traveloger đã checkin" >
                <Image
                    source={(IMAGE.Bike)}
                    style={{
                        width: 40,
                        height: 40
                    }}/>
      
              <Callout onPress={() => { this.props.navigation.navigate('FLDetailSG', {
                                                                              // screen: 'Settings',
                                                                              params: 1 ,
                                                                            })}}>
                <View style={{flexDirection:'column', justifyContent: 'center', alignItems: 'center',marginHorizontal: 10, marginVertical: 15, marginTop: 10}}>
                <Text style={{height:200,width:200}}>
                  <Text style={{fontSize:20,justifyContent: 'center',}}>{listProduct.text}</Text>
                  <Image style={{ height:120, width:200}}  source={{uri: listProduct.photo}} resizeMode='cover' />
                  </Text>
                </View>
              </Callout>   
            </Marker>
              ))}
           
            <MapViewDirections
              origin={origin}
              destination={destination}
              apikey={GOOGLE_API_KEY}
              strokeWidth={5}
              strokeColor= "#0000FF"
            >
            </MapViewDirections>     
            </MapView>
      
      
            <View style={styles.boxInput}>
                <View style={styles.totalBox}>
                  <PlacesInput
                    stylesInput={styles.totalTextInput}
                    stylesList={styles.stylesList}
                    stylesItemText={styles.stylesItemText}
                    // clearQueryOnSelect={true}
                    queryCountries={['vn']}
                    googleApiKey={GOOGLE_API_KEY_PLACE}
                    onSelect={place =>_handlePlace(place)}
                    placeHolder={"Tìm kiếm địa điểm Travelog"}
                  />
                </View>
              </View>
          </View>
          );
        }
      }
      
      
      
      const styles = StyleSheet.create({
        stylesContainer: {
          backgroundColor: 'black'
        },
        boxInput: {
          position: "absolute",
          top: 50,
          left: 0,
          right: 0,
          paddingVertical: 10,
      },
      totalTextInput: {
        height: 45, 
        width: Dimensions.get('window').width - 40,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: '#ffff', 
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        elevation: 5,
        paddingLeft: 50,
        justifyContent: 'center',
      },
      stylesList: {
        width: Dimensions.get('window').width - 40,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: '#ffff',
        marginTop: -4
      },
        container: {
          ...StyleSheet.absoluteFillObject,
          height: 700,
          width: 420,
          justifyContent: 'flex-end',
          alignItems: 'center',
        },
        searchBox: {
          position:'absolute', 
          marginTop: Platform.OS === 'ios' ? 40 : 20, 
          flexDirection:"row",
          backgroundColor: '#fff',
          width: '90%',
          alignSelf:'center',
          borderRadius: 5,
          padding: 10,
          shadowColor: '#ccc',
          shadowOffset: { width: 0, height: 3 },
          shadowOpacity: 0.5,
          shadowRadius: 5,
          elevation: 10,
        },
        map: {
          ...StyleSheet.absoluteFillObject,
        },
       });
