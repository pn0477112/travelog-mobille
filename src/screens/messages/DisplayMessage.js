// components to show messages of recive on right and sender on left side of display

import React, { useContext, useState, useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { UserContext } from "../../context";
import firebase from "firebase/app";
const DisplayMessage = ({ item }) => {
  const { uid } = useContext(UserContext);
  const message = item[1];


  return (
    <>
      {item[0] == "users" ? null : (
        <TouchableOpacity style={styles.container}>
          {message.senderId == uid ? (
            <Text style={styles.rigthText}>{message.message}</Text>
          ) : (
            <Text style={styles.leftText}>{message.message}</Text>
          )}
        </TouchableOpacity>
      )}
    </>
  );
};

export default DisplayMessage;

const styles = StyleSheet.create({
  container: {
    height: 40,
    backgroundColor: "white",
    margin: 5,
  },
  rigthText: {
    position: "absolute",
    right: 5,
    fontSize: 20,
    marginVertical: 2,
  },
  leftText: {
    position: "absolute",
    left: 5,
    fontSize: 20,
    marginVertical: 2,
  },
});
