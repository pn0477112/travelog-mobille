import React, { useState, useContext } from "react";
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity,StatusBar, Dimensions, Image , ActivityIndicator} from 'react-native';
// Firebase
import firebase from "firebase/app";
import { UserContext } from "../../context";

// globalStyles
import { globalStyles } from "../../globalStyles";
import AppButton from "../../components/AppButton";

const Splash = ({ navigation }) => {
    componentDidMount =()=> {
    firebase.auth().onAuthStateChanged(user=>{
        this.props.navigation.navigate(user ? 'Home': 'SignIn')
    })
    return new Promise((resolve) =>
     setTimeout(
       () => { resolve('result') },
       15000
     )
   );
}

  return (
    <View style={styles.container}>
                <StatusBar barStyle="light-content" />
                <View style={styles.header}>
                    {/* <Image
                        source={require('../../image/travelog-logo.png')}
                        style={styles.logo}
                        resizeMode={"center"}
                    /> */}
                     <ActivityIndicator size="large" color="#ecf0f1" />
                </View>

                <View>
                    <View style={styles.button}> 
                     <TouchableOpacity
                     onPress={() => {navigation.navigate('SignIn')}}>
                     <Text>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
  )};

export default Splash;
const {height}= Dimensions.get("screen")
const height_logo = height *0.7 *0.4;
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'#2d3436'
    },

    header: {
       flex:2,
       justifyContent:'center',
       alignItems:'center'
    },  
   
    //  footer: {
    //   flex:1,
    //   backgroundColor: 'white',
    //   borderTopLeftRadius:30,
    //   borderTopRightRadius:30,
    //   paddingVertical:50 
    // },
    logo:{
        width:height_logo,
        height:height_logo
    },

    title:{
        color:'#05375a',
        fontWeight:'bold',
        fontSize: 24,
        marginLeft: 10
    },

    text:{
        color:'gray',
        marginTop:20,
        marginLeft: 15
    },
    
    button:{
        alignItems:'flex-end',
        marginTop:30,

    },

    signIn:{
        width:150,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:50,
        flexDirection:'row',
        marginRight: 20
    },

    textSign:{
        color:'white',
        fontWeight:'bold',
        fontSize:20
    },
});
