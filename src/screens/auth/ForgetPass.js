import React, { useState, useContext } from "react";
import { Alert,View, Text, StyleSheet, ImageBackground, TouchableOpacity, StatusBar, Dimensions, TextInput, AsyncStorage, Image } from 'react-native';

// Firebase
import firebase from "firebase/app";
import { UserContext } from "../../context";

// globalStyles
import { globalStyles } from "../../globalStyles";
import AppButton from "../../components/AppButton";

//
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { useTheme } from 'react-native-paper';


const ForgetPass = ({ navigation }) => {
  const { uid, setUid } = useContext(UserContext);
  const [email, setEmail] = useState("@gmail.com");
  const [password, setPassword] = useState("123456");
  const [check_textInputChange, setCheckInput] = useState();
  const [secureTextEntry, setSecureTextEntry] = useState();

//
const [data, setData] = React.useState({
  username: '',
  password: '',
  check_textInputChange: false,
  secureTextEntry: true,
  isValidUser: true,
  
});

const { colors } = useTheme();
const textInputChange = (val) => {
  if( val.trim().length >= 4 ) {
      setData({
          ...data,
          username: val,
          check_textInputChange: true,
          isValidUser: true
      });
  } else {
      setData({
          ...data,
          username: val,
          check_textInputChange: false,
          isValidUser: false
      });
  }
}

const updateSecureTextEntry = () => {
  setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
  });
}

const handleValidUser = (val) => {
  if( val.trim().length >= 4 ) {
      setData({
          ...data,
          isValidUser: true
      });
  } else {
      setData({
          ...data,
          isValidUser: false
      });
  }
}


//
  const handleSignUp = () => {
    firebase.auth().sendPasswordResetEmail(email)
    .then(function(){
      Alert.alert("Email đã gửi vào hòm thư của bạn.");

    })
    .catch(function(error){
      Alert.alert(error.message);
    })
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleSignUp();
  };
  if (uid) {
    navigation.navigate("AccounStack");
  }

  return (
    <View style={styles.container}>
    <StatusBar backgroundColor='#009387' barStyle="light-content"/>
  <View style={styles.header}>
      <Text style={styles.text_header}>Bạn quên mật khẩu ?</Text>
      <Text style={styles.text_header}>Hãy điền địa chỉ email của bạn !</Text>
  </View>
  <Animatable.View 
      animation="fadeInUpBig"
      style={[styles.footer, {
          backgroundColor: colors.background
      }]}
  >
      <Text style={[styles.text_footer, {
          color: colors.text
      }]}>Email</Text>
      <View style={styles.action}>
          <FontAwesome 
              name="envelope"
              color={colors.text}
              size={20}
          />
          <TextInput 
              placeholder="Nhập Email"
              placeholderTextColor="#666666"
              style={[styles.textInput, {
                  color: colors.text
              }]}
              autoCapitalize="none"
              onChangeText={(val) => textInputChange(val)}
              onChangeText={(val) => setEmail(val)}
              onEndEditing={(e)=>handleValidUser(e.nativeEvent.text)}
              value={email}
          />
          {data.check_textInputChange ? 
          <Animatable.View
              animation="bounceIn"
          >
              <Feather 
                  name="check-circle"
                  color="green"
                  size={20}
              />
          </Animatable.View>
          : null}
      </View>
      { data.isValidUser ? null : 
      <Animatable.View animation="fadeInLeft" duration={500}>
      <Text style={styles.errorMsg}>Hãy điền địa chỉ email của bạn trên 4 ký tự.</Text>
      </Animatable.View>
      }
      <View style={styles.button}>
          <TouchableOpacity
              style={styles.signIn}
              onPress={handleSubmit}
          >
          <LinearGradient
              colors={['#08d4c4', '#01ab9d']}
              style={styles.signIn}
          >
              <Text style={[styles.textSign, {
                  color:'#fff'
              }]}>Xác Nhận</Text>
          </LinearGradient>
          </TouchableOpacity>

      </View>
  </Animatable.View>
</View>
);
};

export default  ForgetPass ;

const styles = StyleSheet.create({
container: {
flex: 1, 
backgroundColor: '#009387'
},
header: {
  flex: 1,
  justifyContent: 'flex-end',
  paddingHorizontal: 20,
  paddingBottom: 50
},
footer: {
  flex: 3,
  backgroundColor: '#fff',
  borderTopLeftRadius: 30,
  borderTopRightRadius: 30,
  paddingHorizontal: 20,
  paddingVertical: 30
},
text_header: {
  color: '#fff',
  fontWeight: 'bold',
  fontSize: 30
},
text_footer: {
  color: '#05375a',
  fontSize: 18
},
action: {
  flexDirection: 'row',
  marginTop: 10,
  borderBottomWidth: 1,
  borderBottomColor: '#f2f2f2',
  paddingBottom: 5
},
actionError: {
  flexDirection: 'row',
  marginTop: 10,
  borderBottomWidth: 1,
  borderBottomColor: '#FF0000',
  paddingBottom: 5
},
textInput: {
  flex: 1,
  marginTop: Platform.OS === 'ios' ? 0 : -12,
  paddingLeft: 10,
  color: '#05375a',
},
errorMsg: {
  color: '#FF0000',
  fontSize: 14,
},
button: {
  alignItems: 'center',
  marginTop: 50
},
signIn: {
  width: '100%',
  height: 50,
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 10
},
textSign: {
  fontSize: 18,
  fontWeight: 'bold'
}
});

