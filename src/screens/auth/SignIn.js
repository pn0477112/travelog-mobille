import React, { useState, useContext } from "react";
import { View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet ,
  StatusBar,
  Alert,Image } from "react-native";
//import {IMAGE} from "../../Image"
// Firebase
import firebase from "firebase/app";
//import { globalStyles } from "../../globalStyles";
import { UserContext } from "../../context";
//import AppButton from "../../components/AppButton";
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { useTheme } from 'react-native-paper';

const SignIn = ({ navigation }) => {
  const { uid, setUid } = useContext(UserContext);
  const [email, setEmail] = useState("phuc15101999@gmail.com");
  const [password, setPassword] = useState("phuc123456");

///
        const [data, setData] = React.useState({
          username: '',
          password: '',
          check_textInputChange: false,
          secureTextEntry: true,
          isValidUser: true,
          isValidPassword: true,
        });

        const { colors } = useTheme();
        const textInputChange = (val) => {
          if( val.trim().length >= 4 ) {
              setData({
                  ...data,
                  username: val,
                  check_textInputChange: true,
                  isValidUser: true
              });
          } else {
              setData({
                  ...data,
                  username: val,
                  check_textInputChange: false,
                  isValidUser: false
              });
          }
        }

        const handlePasswordChange = (val) => {
          if( val.trim().length >= 8 ) {
              setData({
                  ...data,
                  password: val,
                  isValidPassword: true,
              });
              // Alert('Mật khẩu phải dài hơn 8 ký tự');
          } else {
              setData({
                  ...data,
                  password: val,
                  isValidPassword: false
              });
          }
        }

        const updateSecureTextEntry = () => {
          setData({
              ...data,
              secureTextEntry: !data.secureTextEntry
          });
        }

        const handleValidUser = (val) => {
          if( val.trim().length >= 4 ) {
              setData({
                  ...data,
                  isValidUser: true
              });
          } else {
              setData({
                  ...data,
                  isValidUser: false
              });
          }
        }

        const handleValidPassword = (val) => {
            if( val.trim().length >= 8 ) {
                setData({
                    ...data,
                    isValidPassword: true
                });
            } else {
                setData({
                    ...data,
                    isValidPassword: false
                });
            }
          }

        // const loginHandle = (userName, password) => {

        //   const foundUser = Users.filter( item => {
        //       return userName == item.username && password == item.password;
        //   } );

        //   if ( data.username.length == 0 || data.password.length == 0 ) {
        //       Alert.alert('Wrong Input!', 'Username or password field cannot be empty.', [
        //           {text: 'Okay'}
        //       ]);
        //       return;
        //   }

        //   if ( foundUser.length == 0 ) {
        //       Alert.alert('Invalid User!', 'Username or password is incorrect.', [
        //           {text: 'Okay'}
        //       ]);
        //       return;
        //   }
        //   signIn(foundUser);
        // }

  ///

  const handleSignUp = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        setUid(res.user.uid);
        alert("Đăng nhập thành công");
      })
      .catch((error) => {
        alert(error);
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleSignUp();
  };

  if (uid) {
    firebaseApp.auth().onAuthStateChanged(uid=>{
      this.props.navigation.navigate(uid ? 'SignIn': 'Home')
    })
  }

  return (
    <View style={styles.container}>
        <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
            <Text style={styles.text_header}>Chào Mừng Đến Với Travelog!</Text>
        </View>
        <Animatable.View
            animation="fadeInUpBig"
            style={[styles.footer, {
                backgroundColor: colors.background
            }]}
        >
            <Text style={[styles.text_footer, {
                color: colors.text
            }]}>Tài khoản</Text>
            <View style={styles.action}>
                <FontAwesome
                    name="user-o"
                    color={colors.text}
                    size={20}
                />
<<<<<<< HEAD
                <TextInput 
                    placeholder="Nhập tài khoản"
=======
                <TextInput
                    placeholder="Your Username"
>>>>>>> ff6612bb634523700db0505a8297a1dbb4267068
                    placeholderTextColor="#666666"
                    style={[styles.textInput, {
                        color: colors.text
                    }]}
                    autoCapitalize="none"
                    onChangeText={(val) => textInputChange(val)}
                    onChangeText={(val) => setEmail(val)}
                    onEndEditing={(e)=>handleValidUser(e.nativeEvent.text)}
                    value={email}
                />
                {data.check_textInputChange ?
                <Animatable.View
                    animation="bounceIn"
                >
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>
                : null}
            </View>
            { data.isValidUser ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Hãy nhập tên tài khỏan trên 4 ký tự.</Text>
            </Animatable.View>
            }


            <Text style={[styles.text_footer, {
                color: colors.text,
                marginTop: 35
            }]}>Mật Khẩu</Text>
            <View style={styles.action}>
                <Feather
                    name="lock"
                    color={colors.text}
                    size={20}
                />
                <TextInput 
                    placeholder="Nhập Mật Khẩu"
                    placeholderTextColor="#666666"
                    secureTextEntry={data.secureTextEntry ? true : false}
                    style={[styles.textInput, {
                        color: colors.text
                    }]}
                    autoCapitalize="none"
                    onChangeText={(val) => handlePasswordChange(val)}
                    onChangeText={(val) => setPassword(val)}
                    onEndEditing={(e)=>handleValidPassword(e.nativeEvent.text)}
                    value={password}
                />
                <TouchableOpacity
                    onPress={updateSecureTextEntry}
                >
                    {data.secureTextEntry ?
                    <Feather
                        name="eye-off"
                        color="grey"
                        size={20}
                    />
                    :
                    <Feather
                        name="eye"
                        color="grey"
                        size={20}
                    />
                    }
                </TouchableOpacity>
            </View>
            { data.isValidPassword ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Hãy nhập mật khẩu trên 8 ký tự.</Text>
            </Animatable.View>
            }


            <TouchableOpacity onPress={() => navigation.navigate("ForgetPass")} >
                <Text style={{color: '#009387', marginTop:15}}>Quên mật khẩu?</Text>
            </TouchableOpacity>
            <View style={styles.button}>
                <TouchableOpacity
                    style={styles.signIn}
                    onPress={handleSubmit}
                >
                <LinearGradient
                    colors={['#08d4c4', '#01ab9d']}
                    style={styles.signIn}
                >
                    <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>Đăng Nhập</Text>
                </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.navigate("SignUp")}
                    style={[styles.signIn, {
                        borderColor: '#009387',
                        borderWidth: 1,
                        marginTop: 15
                    }]}
                >
                    <Text style={[styles.textSign, {
                        color: '#009387'
                    }]}>Đăng Ký</Text>
                </TouchableOpacity>
            </View>
        </Animatable.View>
      </View>
    );
};

export default SignIn;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#009387'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
  });
