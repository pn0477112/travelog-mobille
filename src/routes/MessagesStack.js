import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MessagesList from "../screens/messages/MessagesList";
import NewMessages from "../screens/messages/NewMessages";
import Messages from "../screens/messages/Messages";
const Stack = createStackNavigator();
const MessagesStack = () => {
  return (
    <Stack.Navigator
    initialRouteName="MessagesList"
    headerMode="screen"
    screenOptions={{
      headerTintColor: 'white',
      headerStyle: { backgroundColor: 'black' }}}
    >
      <Stack.Screen name="MessagesList" component={MessagesList} 
      options={{
        title: 'Tin nhắn'
      }}/>
      <Stack.Screen name="NewMessages" component={NewMessages} 
      options={{
        title: 'Tin nhắn mới'
      }}/>
      <Stack.Screen name="Messages" component={Messages} 
      options={{
        title: 'Tin nhắn'
      }} />
    </Stack.Navigator>
  );
};

export default MessagesStack;
