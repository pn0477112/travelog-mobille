import React, { useContext } from "react";
import { UserContext } from "../context";

import { createStackNavigator } from "@react-navigation/stack";

import UserAccountDetails from "../screens/accountDetails/UserAccountDetails";
import EditUserDetails from "../screens/accountDetails/EditUserDetails";
import AddPost from "../screens/accountDetails/AddPost";
import AddFriends from "../screens/accountDetails/AddFriends";
import FriendsList from "../screens/accountDetails/FriendsList";
import ChangePassword from "../screens/accountDetails/ChangePassword"
import AppButton from "../components/AppButton";
import { Button } from 'react-native-paper';
const Stack = createStackNavigator();
const AccounStack = () => {
  const { setUid } = useContext(UserContext);

  return (
    <Stack.Navigator
    // screenOptions={{
    //   headerRight: () => (
    //     <Button
    //       onPress={() => alert("This is a button!")}
    //       title="Info"
    //       color="#fff"
    //     />
    //   ),
    // }}
    >
      <Stack.Screen
        name="Cá nhân"
        component={UserAccountDetails}
        initialRouteName="SignIn"
        headerMode="screen"
        screenOptions={{
          headerTintColor: 'black',
          headerStyle: { backgroundColor: 'black' },
        }}
        options={{
          headerTintColor: 'white',
          headerStyle: { backgroundColor: 'black' },
          headerRight: () => (
            <Button icon="arrow-right-bold" mode="contained" onPress={() => setUid(null)}>
              Đăng xuất
            </Button>
            // <AppButton title="Sign Out" />
          ),
        }}
      />
      <Stack.Screen name="EditUserDetails" component={EditUserDetails}
              options={{
                title: 'Chỉnh sửa thông tin'
              }} />
      <Stack.Screen name="AddPost" component={AddPost} 
              options={{
                title: 'Thêm bài viết'
              }} />
      <Stack.Screen name="AddFriends" component={AddFriends} 
              options={{
                title: 'Thêm bạn bè'
              }}/>
      <Stack.Screen name="FriendsList" component={FriendsList} 
              options={{
                title: 'Danh sách bạn bè'
              }} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} 
              options={{
                title: 'Đổi mật khẩu'
              }} />
    </Stack.Navigator>
  );
};

export default AccounStack;
