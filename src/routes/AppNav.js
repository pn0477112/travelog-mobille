import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeStack from "./HomeStack";
import AccounStack from "./AccountStack";
import GuideStack from "./GuideStack";
import MessagesStack from "./MessagesStack";
import MapsStack from "./MapsStack";
const Tab = createBottomTabNavigator();

const AppNav = () => {
  return (
    <Tab.Navigator       
    initialRouteName="HomeStack"
    tabBarOptions={{
      activeTintColor: '#2d3436',
    }}>
      <Tab.Screen name="Home" component={HomeStack}  options={{
          tabBarLabel: 'Trang chủ',
          tabBarIcon: ({ color, size}) => (
            <MaterialCommunityIcons name="home-variant" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Guide" component={GuideStack} options={{
          tabBarLabel: 'Mẹo',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="approximately-equal-box" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Messages" component={MessagesStack} options={{
          tabBarLabel: 'Nhắn tin',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="facebook-messenger" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Map" component={MapsStack} options={{
          tabBarLabel: 'Bản đồ',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="google-maps" color={color} size={size} />
          ),
        }}/>
      <Tab.Screen name="Accoun" component={AccounStack} options={{
          tabBarLabel: 'Cá nhân',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}/>

    </Tab.Navigator>
  );
};

export default AppNav;
