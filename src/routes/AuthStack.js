import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import SignIn from "../screens/auth/SignIn";
import SignUp from "../screens/auth/SignUp";
import ForgetPass from "../screens/auth/ForgetPass";
import Splash from "../screens/auth/Splash";
// import {IMAGE} from '../constants/Image';
const Stack = createStackNavigator();
const AuthStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="SignIn"
      headerMode="none"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: { backgroundColor: '#009387' },
      }}>
      <Stack.Screen name="SignIn" component={SignIn}
        options={{
          title: null
        }} />
      <Stack.Screen name="SignUp" component={SignUp}
        options={{
          title: null
        }} />
      <Stack.Screen name="ForgetPass" component={ForgetPass}
        options={{
          title: null
        }}/>
      <Stack.Screen name="Splash" component={Splash}
        options={{
          title: null
        }}/>
    </Stack.Navigator>
  );
};

export default AuthStack;
