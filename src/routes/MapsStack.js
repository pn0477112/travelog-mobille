import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Map from "../screens/maps/Map";
const Stack = createStackNavigator();
const MapsStack = () => {
  return (
    <Stack.Navigator
    initialRouteName="Map"
    headerMode="screen"
    screenOptions={{
      headerTintColor: 'white',
      headerStyle: { backgroundColor: 'black' },
    }}>
      <Stack.Screen name="Map" component={Map} options={{
        title: 'Địa điểm Travelog'
      }}/>
    </Stack.Navigator>
    
  );
};

export default MapsStack;
