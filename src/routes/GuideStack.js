import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Guide from "../screens/guide/Guide";
import GuideCategory from "../screens/guide/GuideCategory";
import ChupAnh from "../screens/guide/ChupAnh";
import Sea from "../screens/guide/Sea";
import Protect from "../screens/guide/Protect";
import Jungle from "../screens/guide/Jungle";
import Jungle2 from "../screens/guide/Jungle2";
import Phuot from "../screens/guide/Phuot";
import MapsCategory  from "../screens/guide/MapsCategory";
import FLDaLat from "../screens/guide/FLDaLat";
import FLDetailDL from "../screens/guide/FLDetailDL";
import FLSaiGon from "../screens/guide/FLSaiGon";
import FLDetailSG from "../screens/guide/FLDetailSG";
import FLHaNoi from "../screens/guide/FLHaNoi";
import FLDetailHN from "../screens/guide/FLDetailHN";
import FLSapa from "../screens/guide/FLSapa";
import FLDetailSP from "../screens/guide/FLDetailSP";
import FLVungTau from "../screens/guide/FLVungTau";
import FLDetailVT from "../screens/guide/FLDetailVT";
import Panorama from "../screens/guide/Panorama";
import Map from "../screens/guide/Map";
import MapAll from "../screens/guide/MapAll";
const Stack = createStackNavigator();
const GuideStack= () => {
  return (
    <Stack.Navigator
    initialRouteName="Guide"
    headerMode="screen"
    screenOptions={{
      headerTintColor: 'white',
      headerStyle: { backgroundColor: 'black' },
    }}>
      <Stack.Screen name="Guide" component={Guide} 
       options={{
        title: 'Mẹo'
      }}/>
      <Stack.Screen name="GuideCategory" component={GuideCategory} 
       options={{
        title: 'Mẹo du lịch'
      }}/>
       <Stack.Screen name="ChupAnh" component={ChupAnh} 
       options={{
        title: 'Mẹo chụp ảnh'
      }}/>
        <Stack.Screen name="Sea" component={Sea} 
       options={{
        title: 'Mẹo đi biển'
      }}/>
       <Stack.Screen name="Protect" component={Protect} 
       options={{
        title: 'Mẹo bảo vệ bản thân'
      }}/>
      <Stack.Screen name="Jungle" component={Jungle} 
       options={{
        title: 'Mẹo đi rừng'
      }}/>
      <Stack.Screen name="Jungle2" component={Jungle2} 
       options={{
        title: 'Tin tức sự kiện'
      }}/>
      <Stack.Screen name="Phuot" component={Phuot} 
       options={{
        title: 'Mẹo đi Phượt'
      }}/>

      <Stack.Screen name="MapsCategory" component={MapsCategory} 
            options={{
              title: 'Địa điểm du lịch nổi bật'
            }}/>
       <Stack.Screen name="FLDaLat" component={FLDaLat} 
       options={{
        title: 'Du lịch Đà Lạt'
      }}/>
      <Stack.Screen name="FLDetailDL" component={FLDetailDL} 
       options={{
        title: 'Địa điểm du lịch Đà Lạt'
      }}/>
      <Stack.Screen name="FLSaiGon" component={FLSaiGon} 
       options={{
        title: 'Du lịch Sài Gòn'
      }}/>
      <Stack.Screen name="FLDetailSG" component={FLDetailSG} 
       options={{
        title: 'Địa điểm du lịch Sài Gòn'
      }}/>
      <Stack.Screen name="FLSapa" component={FLSapa} 
       options={{
        title: 'Du lịch Sapa'
      }}/> 
      <Stack.Screen name="FLDetailSP" component={FLDetailSP} 
       options={{
        title: 'Địa điểm du lịch Sapa'
      }}/> 
      <Stack.Screen name="FLHaNoi" component={FLHaNoi} 
       options={{
        title: 'Du lịch Hà Nội'
      }}/> 
      <Stack.Screen name="FLDetailHN" component={FLDetailHN} 
       options={{
        title: 'Địa điểm du lịch Hà Nội'
      }}/> 
      <Stack.Screen name="FLVungTau" component={FLVungTau} 
       options={{
        title: 'Du lịch Vũng Tàu'
      }}/> 
      <Stack.Screen name="FLDetailVT" component={FLDetailVT} 
       options={{
        title: 'Địa điểm du lịch Vũng Tàu'
      }}/>  
      <Stack.Screen name="Panorama" component={Panorama} 
       options={{
        title: 'Panorama địa điểm'
      }}/> 
      <Stack.Screen name="Map" component={Map} 
       options={{
        title: 'Bản đồ'
      }}/> 
      <Stack.Screen name="MapAll" component={MapAll} 
       options={{
        title: 'Bản đồ'
      }}/> 
    </Stack.Navigator>
    
  );
};

export default GuideStack;