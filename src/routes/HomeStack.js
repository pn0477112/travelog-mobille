import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "../screens/home/Home";

const Stack = createStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator
    initialRouteName="Home"
    headerMode="screen"
    screenOptions={{
      headerTintColor: 'white',
      headerStyle: { backgroundColor: 'black' },
    }}>
      <Stack.Screen name="Home" component={Home} 
       options={{
        title: 'Trang chủ'
      }}/>
    </Stack.Navigator>
  );
};

export default HomeStack;
