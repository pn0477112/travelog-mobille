const Light = {
    theme: {
      background: '#ededed',
      border: '#bdbdbd',
      backgroundAlt: '#eaeaeb',
      borderAlt: '#bdbdbd',
      text: '#171717'
    }
  }
  export default Light