import * as React from 'react';
import { Image } from 'react-native';
import { Banner } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const BannerComponet = () => {
  const [visible, setVisible] = React.useState(true);

  return (
    <Banner
      visible={visible}
      actions={[
        {
          label: 'Đóng',
          onPress: () => setVisible(false),
        },
        {
          label: 'Tìm hiểu thêm',
          onPress: () => setVisible(false),
        },
      ]}
      icon={({size, color}) => (
        <MaterialCommunityIcons name="alert-circle" color={color} size={size} />
      )}>
      Để sử dụng vui lòng kiểm tra kết nối mạng
    </Banner>
  );
};

export default BannerComponet;