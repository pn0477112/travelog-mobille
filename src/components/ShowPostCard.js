// A components to show post that take postId by props then fetch post details from database and show

import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, View, Image } from "react-native";
import { Card , Text, Title, Paragraph} from 'react-native-paper';

// firebase
import firebase from "firebase/app";
import moment, { isMoment } from 'moment';
// Global StylesSheet
import { globalStyles } from "../globalStyles";

// Context
import { UserContext } from "../context";

const ShowPostCard = ({ postId }) => {
  const { uid } = useContext(UserContext);

  const [postData, setPostData] = useState(null);
  const [postOwnerDetails, setPostOwnerDetails] = useState(null);

  // Function to fetch post details form database
  const getPostData = async () => {
    await firebase
      .database()
      .ref("/posts/" + postId)
      .once("value", (snapshot) => setPostData(snapshot.val()));
  };

  // function to fecth details of post owner
  const getpostOwnerDetails = async () => {
    await firebase
      .database()
      .ref("/usersPublicDetails/" + postData.uid)
      .once("value", (snapshot) => setPostOwnerDetails(snapshot.val()));
  };

  useEffect(() => {
    if (postData) {
      if (postData.uid != uid) {
        getpostOwnerDetails();
      }
    }
  }, [postData]);
  useEffect(() => {
    getPostData();
  }, []);

  return (
    <View style={styles.cardContainer}>
      {postData ? (
        <>
          {postOwnerDetails ?
            (
            <View style={styles.profileContainer}>
               
              <View style={styles.profileImageContainer}>
                <Card>
                  <Card.Cover source={{ uri: postOwnerDetails.profileImageUrl }}
                  style={globalStyles.image} 
                  resizeMode="cover"/>
                </Card>
                <Image
                  source={{ uri: postOwnerDetails.profileImageUrl }}
                  style={globalStyles.image}
                  resizeMode="cover"
                />
              </View>
              <Text style={styles.profileText}>{postOwnerDetails.name}</Text>
              <Paragraph style={{color: '#b2bec3'}}>{postData.currentDate}</Paragraph>
              
            </View>
          ) : null}
          <View style={styles.postImageContainer}>
          <Text style={styles.caption}>{postData.caption}</Text>

            <Image
              source={{ uri: postData.postImageUrl }}
              style={globalStyles.image}
              resizeMode="contain"
            />
            
            <View style={{alignItems:'baseline', justifyContent:'space-between', marginStart: 15}}> 
              <Paragraph style={{color: '#b2bec3'}} onPress={()=>console.log('liked')}>Like</Paragraph>
              {/* <Paragraph style={{color: '#b2bec3'}}>Comment</Paragraph> */}
            </View>

          </View>

        </>
      ) : null}
    </View>
  );
};

export default ShowPostCard;
const styles = StyleSheet.create({
  cardContainer: {
    alignContent: "center",
    backgroundColor: "white",
    margin: 5,
    borderRadius: 15,
    flexDirection: "column",
  },
  caption: {
    marginBottom: 10,
    marginLeft: 30
  },

  cardFooter: {
    position: "absolute",
    bottom: 5,
    height: 60,
    alignItems: "flex-end",
    justifyContent: "space-around",
  },

  postImageContainer: {
    height: 250,
    width: '100%',
    alignItems: 'stretch',
    marginTop: 10,
    marginBottom: 75,

  },

  profileText: {
    marginHorizontal: 12,
  },
  profileContainer: {
    flexDirection: "row",
    alignItems: "center",
    height: 70,
  },
  profileImageContainer: {
    height: 50,
    width: 50,
    borderRadius: 25,
    overflow: "hidden",
    marginLeft: 10,
  },
});
