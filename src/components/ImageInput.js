// A image Input Component

import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Alert,
  SafeAreaView,
  TouchableOpacity,
  Text
} from "react-native";

// import { MaterialCommunityIcons } from "@expo/vector-icons";
// import * as ImagePicker from "expo-image-picker";
import ImagePicker from 'react-native-image-picker'
const ImageInput = ({ imageUri, setImageUri }) => {
  const requestPermission = async () => {
    const { granted } = await ImagePicker.requestCameraRollPermissionsAsync();
    if (!granted) alert("You need to enable permission to access the library.");
  };

  const handlePress = () => {
    if (!imageUri) selectImage();
    else
      Alert.alert("Delete", "Are you sure you want to delete this image?", [
        { text: "Yes", onPress: () => setImageUri(null) },
        { text: "No" },
      ]);
  };

  const selectImage = async () => {
    // try {
      // const result = await ImagePicker.launchImageLibraryAsync({
      //   mediaTypes: ImagePicker.MediaTypeOptions.Images,
      //   quality: 0.5,
      // });
      let options = {
        title: 'Select Image',
        customButtons: [
          {
            name: 'customOptionKey',
            title: 'Choose Photo from Custom Option'
          },
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
       ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log(
            'User tapped custom button: ',
            response.customButton
          );
          alert(response.customButton);
        } else {
          let source = response;
          // You can also display the image using data:
          // let source = {
          //   uri: 'data:image/jpeg;base64,' + response.data
          // };
          setImageUri(source.uri)
        }
      });
    };
  //   } catch (error) {
  //     alert("Error reading an image", error);
  //   }
  // };

  useEffect(() => {
    requestPermission();
  }, []);
      
  return (

     <TouchableWithoutFeedback onPress={handlePress}>
      <View style={styles.container}>
        {!imageUri && (
          <Text>Upload Image</Text>
        )}
        {imageUri && <Image source={{ uri: imageUri }} style={styles.image} />}
      </View>
    </TouchableWithoutFeedback>
  );
};
export default ImageInput;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15,
    height: 200,
    justifyContent: "center",
    marginVertical: 10,
    overflow: "hidden",
    width: 200,
  },
  image: {
    height: "100%",
    width: "100%",
  },
});
