-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 14, 2021 lúc 12:44 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `travelogdb`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `diadiem`
--

CREATE TABLE `diadiem` (
  `id` int(11) NOT NULL,
  `text` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `Recommend` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `photo` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `latitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `longitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `panorama` text CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `diadiem`
--

INSERT INTO `diadiem` (`id`, `text`, `Description`, `Recommend`, `photo`, `address`, `latitude`, `longitude`, `panorama`) VALUES
(1, 'Nhà Thờ Đức Bà Mới Sửa Lần 2\r\n', 'Là nhà thờ chính toà của Tổng Giáo Phận Thành Phố Hồ Chí Minh  , một trong những công trình kiến trúc độc đáo của Sài Gòn , điểm đến của du khách trong và ngoài nước, nét đặc trưng của du lịch Việt Nam. Tên gọi ban đầu của nhà thờ là Nhà thờ Sài Gòn, tên gọi Nhà thờ Đức Bà bắt đầu được sử dụng từ năm 1959.', 'Bạn có thể đến Nhà thờ Đức Bà vào bất kỳ lúc nào trong ngày, kể cả vào buổi tối vẫn sẽ có rất đông người tham quan và \"đội ngũ\" quà vặt, cà phê cho bạn chọn lựa.Nếu không tham gia các lễ, bạn cần 90 phút đến 2 tiếng để tham quan, chụp ảnh và thưởng thức các món ăn ngon lành nơi đây.', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/2-min-20-750x597.jpg', 'Quảng trường Công xã Paris, quận 1, TP.HCM', '10.780081', '106.699019', 'http://www.360cities.net/image/notre-dame-cathedal-saigon-vietnam#11.40,-29.60,70.0'),
(2, 'Phố Đi Bộ Nguyễn Huệ', 'Đường Nguyễn Huệ là một đường phố trung tâm tại Quận 1, Thành phố Hồ Chí Minh, nối liền Trụ sở Ủy ban Nhân dân Thành phố với bến Bạch Đằng, bờ sông Sài Gòn.\',\r\n    ', 'Phố đi bộ Nguyễn Huệ hoạt động hầu hết các ngày, từ thứ Hai đến Chủ nhật. Nhưng nếu muốn hòa mình vào không gian náo nhiệt thì hãy chọn những ngày cuối tuần nhé. Thường các lễ hội, các sự kiện quan trọng đều được tổ chức các ngày này để phục vụ nhu cầu vui chơi, giải trí của du khách.', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/4-min-25-750x500.jpg', 'Nguyễn Huệ, Bến Nghé, Quận 1, Hồ Chí Minh', '10.775676', '106.700226', 'https://www.360cities.net/image/pho-di-bo-1'),
(3, 'Dinh Độc Lập', 'Dinh Độc Lập (tên gọi trước đây là dinh Norodom) là một công trình kiến trúc, tòa nhà ở Thành phố Hồ Chí Minh. Đây từng là nơi ở và làm việc của Tổng thống Việt Nam Cộng hòa. Hiện nay, đã được chính phủ Việt Nam xếp hạng là di tích quốc gia đặc biệt.', 'Di tích lịch sử Dinh Độc Lập mở cửa bán vé phục vụ du khách tham quan hàng ngày (kể cả cuối tuần và các dịp lễ, Tết),Sáng: 7h30 – 11h00,Chiều: 13h00 – 16h00', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/1-min-18-750x370.jpg', '135 Nam Kỳ Khởi Nghĩa, Phường Bến Thành, Quận 1, Hồ Chí Minh', '10.777449', '106.695491', 'https://www.360cities.net/image/hcm-mausoleum-ii'),
(4, 'Chợ Bến Thành', 'Chợ Bến Thành là một ngôi chợ nằm tại quận 1, Thành phố Hồ Chí Minh. Chợ được khởi công xây dựng từ năm 1912 đến cuối tháng 3 năm 1914 thì hoàn tất. Trong nhiều trường hợp, hình ảnh đồng hồ ở cửa nam của ngôi chợ này được xem là biểu tượng không chính thức của Thành phố Hồ Chí Minh.', 'Chợ bắt đầu mở cửa hoạt động từ 4h sáng mỗi ngày trong tuần.Bắt đầu từ 7h00 tối, hơn 170 quầy hàng đổ ra 2 con đường Phan Bội Châu và Phan Chu Trinh bên hông chợ với các quầy mỹ nghê, lưu niệm, quần áo và đặc biệt là hàng ăn với đủ loại đồ nướng, hải sản, bia và các đặc sản', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/3-min-21-750x563.jpg', 'Chợ, Đường Lê Lợi, Phường Bến Thành, Quận 1, Hồ Chí Minh', '10.772352', '106.698257', 'https://www.360cities.net/image/ben-thanh-market-vietnam'),
(5, 'Nhà hát thành phố', 'Nằm ở điểm cuối đường Lê Lợi, tọa lạc trên con đường Ðồng Khởi – trung tâm thành phố Hồ Chí Minh, bên cạnh là hai khách sạn lớn Caravelle và Continental. Nhà hát được xem như một công trình văn hóa tiêu biểu và tốn kém nhất ở Sài Gòn thời Pháp thuộc.', 'Nhà hát Thành phố Hồ Chí Minh nằm ngay trung tâm Quận 1 và chỉ mở cửa cho khách tham gia các chương trình nghệ thuật. Có thể mua vé dễ dàng tại phòng vé cạnh nhà hát.Từ nhà hát, du khách có thể dễ dàng đi bộ đến nhiều danh thắng nổi tiếng khác trong khu vực như Nhà thờ Đức Bà Sài Gòn, Dinh Độc Lập. Những con phố quanh nhà hát rợp bóng cây, rất phù hợp cho du khách chầm chậm tản bộ, ngắm cảnh', 'https://media.gody.vn/images/ho-chi-minh/nha-hat-thanh-pho/10-2016/20161021024545-nha-hat-tp-gody%20(8).jpg', '07 Công Trường Lam Sơn, Bến Nghé, Quận 1, Hồ Chí Minh', '10.776923', '106.703148', 'https://www.360cities.net/image/saigon-theater'),
(6, 'Địa Đạo Củ Chi', 'Địa đạo Củ Chi là một hệ thống phòng thủ trong lòng đất ở huyện Củ Chi, cách Thành phố Hồ Chí Minh 70 km về hướng tây-bắc. Hệ thống này được quân kháng chiến Việt Minh và Mặt trận Dân tộc Giải phóng miền Nam Việt Nam đào trong thời kỳ Chiến tranh Đông Dương và Chiến tranh Việt Nam. Hệ thống địa đạo bao gồm bệnh xá, nhiều phòng ở, nhà bếp, kho chứa, phòng làm việc, hệ thống đường ngầm dưới lòng đất, dài khoảng 250 km và có các hệ thống thông hơi tại vị trí các bụi cây.', 'Địa đạo Củ Chi được xây dựng trên vùng “đất thép” Củ Chi, cách trung tâm TP.Hồ Chí Minh khoảng 70 km. Các bạn có thể đi xe máy, xe bus hoặc taxi đến đây', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/6-min-3.png', 'thuộc ấp Phú Hiệp, xã Phú Mỹ Hưng, huyện Củ Chi.', '11.062408', '106.529460', NULL),
(7, 'Bảo Tàng Lịch Sử Việt Nam', 'Bảo tàng Lịch sử thành phố Hồ Chí Minh tọa lạc tại số 2 đường Nguyễn Bỉnh Khiêm, phường Bến Nghé, Quận 1, bên cạnh Thảo Cầm Viên Sài Gòn. Đây là nơi bảo tồn và trưng bày hàng chục ngàn hiện vật quý được sưu tầm trong và ngoài nước Việt Nam', 'Nếu muốn tham quan bảo tàng lịch sử Việt Nam tại TP HCM, du khách có thể lựa chọn các Tour Sài Gòn của Viet Fun Travel. Chùm Tour Sài Gòn của chúng tôi có nhiều Tour hấp dẫn để quý khách lựa chọn như Tour Sài Gòn – TP HCM Nửa Ngày, Tour Tham Quan Sài Gòn – TP HCM Nửa Ngày hoặc Tour Tham Quan Sài Gòn – TP HCM 1 Ngày… Khi tham gia các Tour này, du khách sẽ dễ dàng tham quan Bảo tàng lịch sử Việt Nam tại Thành phố Hồ Chí Minh', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/5-min-19.jpg', '2 Nguyễn Bỉnh Khiêm, Bến Nghé, Quận 1, Hồ Chí Minh', '10.788247', '106.704705', 'https://www.360cities.net/image/vietnam-history-museum-1?fbclid=IwAR0Kvz2n-mRYYMoeX_r6--vHLk2KNoHHK4xTDlsAs0SiXJwGRjh8nnWoS_c'),
(8, 'Bảo Tàng Áo Dài', 'Bảo tàng áo dài, là 1 trong 2 bảo tàng tư nhân tại TP.HCM, đây là một tâm huyết của hoạ sỹ - nhà thiết kế Sỹ Hoàng với mong muốn giới thiệu quảng bá và bảo tồn vẻ đẹp của loại trang phục có bề dày hơn 300 năm lịch sử của người Việt. Tại đây được trưng bày rất nhiều, rất nhiều áo dài từ truyền thống đến hiện đại cách tân và các áo dài do NTK Sỹ Hoàng thiết kế trong các cuộc thi hoa hậu hoặc các người đẹp dự thi hoa hậu tặng lại bảo tàng. Tham quan bảo tàng cũng sẽ giúp bạn hiểu được bản sắc văn hóa dân tộc của nước ta từ xa xưa', 'ảo tàng mở cửa từ 8h30 -16h30 mỗi ngày. Giá vé: 30.000đ -100.000đ (trẻ em dưới 2 tuổi miễn phí vé).Sinh viên, học sinh giá vé 30.000đ.Thông tin để các bạn có thể đến và tham quan', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/9-min-15-750x788.jpg', '206/19/30 Long Thuận, Long Phước, Quận 9, Hồ Chí Minh', '10.808657', '106.853143', 'https://www.360cities.net/image/a-graduation-party-with-33-ao-dai-vietnam-at-hanoi-university?fbclid=IwAR1roAz2JBZ4tIQXLZV-meF7bQbtIX-ybhL1Aq-IJW8Cn7GfEKO2sQ7Dczo'),
(9, 'Đường Sách', 'Không chỉ là một điểm thu hút những người yêu sách mà đây còn trở thành tụ điểm gặp gỡ và thư giãn của nhiều người dân, đặc biệt là các bạn trẻ vào dịp cuối tuần', 'Nếu là một người yêu sách thì đường sách Nguyễn Văn Bình sẽ là một địa điểm bạn không thể bỏ qua khi du lịch Sài Gòn. Hãy đến đây và hoà mình vào không gian yên bình, thư thái giữa Sài Gòn hối hả, xô bồ tấp nập, chắc chắn bạn có sẽ những trải nghiệm vô cùng thú vị ', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/8-min-17-750x562.jpg', 'Đường Nguyễn Văn Bình, Bến Nghé, Quận 1, Hồ Chí Minh', '10.780338', '106.699265', 'https://zingnews.vn/sai-gon-qua-ong-kinh-gear-360-post710117.html'),
(10, 'Bảo Tàng Y Học Cổ Truyền', 'Nằm nép mình ở một góc đường Hoàng Dư Khương tại địa chỉ số 41, quận 10, thành phố Hồ Chí Minh là FiTo Museum – một bảo tàng tư nhân về y học cổ truyền đầu tiên ở Việt Nam.Bảo tàng ra đời từ ý tưởng, lòng đam mê và quá trình sưu tập trong nhiều năm của ông Lê Khắc Tâm, một người làm việc trong ngành dược phẩm trong mấy chục năm qua.', 'Giờ mở cửa: 08h30 – 17h30 tất cả các ngày trong tuần. Giá vé: 50.000đ/ người lớn', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/10-min-13-750x422.jpg', '41 Hoàng Dư Khương, Phường 12, Quận 10, Hồ Chí Minh', '10.776393', '106.671791', NULL),
(11, 'Bảo Tàng Mỹ Thuật Thành Phố\'', 'Bảo tàng Mỹ thuật Thành phố Hồ Chí Minh tọa lạc tại số 97 Phó Đức Chính, Quận 1, Thành phố Hồ Chí Minh, Việt Nam; được thành lập năm 1987 và đi vào hoạt động năm 1991. Tòa nhà bao gồm 3 tầng, trưng bày các tác phẩm hội họa, điêu khắc, cổ vật có giá trị mỹ thuật cao. Đây là một trong những trung tâm mỹ thuật lớn nhất nước.', 'Giá vé: Trẻ em: 3.000 đồng/người. Người lớn: 10.000 đồng/người.Học sinh, sinh viên, quân nhân, các đối tượng chính sách được miễn phí hoặc giảm giá vé tùy theo từng trường hợp cụ thể.Phần lớn khách tham quan thường tới đây vào những dịp cuối tuần, ngày nghỉ lễ. Không chỉ có những người Việt tới thăm quan mà cũng có rất đông khách du lịch đến từ nước ngoài. Nhiều đoàn học sinh, sinh viên cũng chọn địa điểm này là nơi sinh hoạt ngoại khóa hoặc tham khảo thêm môn học nghệ thuật', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/12-min-4-750x500.jpg', '97A Phó Đức Chính, Phường Nguyễn Thái Bình, Quận 1, Hồ Chí Minh', '10.770219', '106.699405', NULL),
(12, 'Thảo Cầm Viên', 'Thảo Cầm Viên Sài Gòn (tên gọi tắt: Thảo Cầm Viên, người dân quen gọi Sở thú) là công viên bảo tồn động vật - thực vật ở Thành phố Hồ Chí Minh, Việt Nam. Đây là vườn thú có tuổi thọ đứng hàng thứ 8 trên thế giới.[1] Khuôn viên rộng lớn này hiện tọa lạc gần hạ lưu kênh Nhiêu Lộc - Thị Nghè với hai cổng vào nằm ở số 2B đường Nguyễn Bỉnh Khiêm và số 1 đường Nguyễn Thị Minh Khai phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh', 'Thảo Cầm Viên mở cửa từ thứ Hai đến CN với 2 khung giờ. 5h00 đến 6h30: mở cửa cho người dân tham gia tập dưỡng sinh, tập thể dục.7h đến 18h30: mở cửa dành cho khách tham quan. Các ngày trong tuần, thứ 7, chủ nhật và các ngày lễ tết', 'https://sayhi.vn/blog/wp-content/uploads/2019/08/11-min-5-750x446.jpg', '2 Nguyễn Bỉnh Khiêm, Bến Nghé, Quận 1, Hồ Chí Minh\'', '10.788313', '106.705055', 'https://www.360cities.net/image/botanical-garden-view-1'),
(13, 'Lanmark 81', 'Landmark 81, tên chính thức Vincom Landmark 81, là một tòa nhà chọc trời trong tổ hợp dự án Vinhomes Central Park, một dự án có tổng mức đầu tư khoảng 300 triệu USD, do Công ty Cổ phần Đầu tư xây dựng Tân Liên Phát thuộc Vingroup làm chủ đầu tư. Tòa tháp cao 81 tầng (với 3 tầng hầm), hiện tại là tòa nhà cao nhất Việt Nam, cao thứ 2 Đông Nam Á, đứng thứ 14 thế giới vào thời điểm hoàn thiện tháng 7 năm 2018. Dự án được xây dựng ở Tân Cảng, quận Bình Thạnh, ven sông Sài Gòn được khởi công ngày 26/07/2014. Tòa nhà được khai trương và đưa vào sử dụng ngày 26/07/2018.', 'Nên đi Landmark 81 SkyView vào buổi chiều tối để ngắm hoàng hôn hoặc Sài Gòn buổi tối sẽ rất lung linh, ấn tượng.Nên kết hợp đi mua sắm hoặc giải trí ở các tầng dưới của Landmark 81 luôn cho tiện. Đặc biệt ở tầng B1 có khu vực trượt băng hoặc rạp chiếu phim CGV', 'https://tvlk-prod-asset.imgix.net/v2/generic/C8Kj5%2FdCnE75BMxGpYYdcJVuEQbV3CnmF8ONfpQKM2nSWWBIIZYawS1qMa7rEFnffYG%2Bi9OZUEt8PuKdInSV4NhZ1LlG4iuV6rp4mp8FO2uJwIFDbz%2FtKBkJl3SKoObz3Xz3x%2FnYAfcq%2BWVIq6QecI4HpNOAmj0Pq8TA0GvRcvZ51ec9s%2BNRDwS9olJGFuR%2FYO4ApUCkFvMe2EIoHCdb4zs9GFSVTdgeVXYdd%2F4l5i9YrJ7BcrUZIdkLQQ8LOHxyfP9IpmvV5Oy5hmN5PMpH4klHEMTacVKGtfN8IGUVinuaFpGpj9zEFjaEw7NdwLwjorsd6k7plyCrS3ROWU6tSA%3D%3D?auto=compress%2Cformat&cs=srgb&ixlib=java-1.1.12&q=30&tv-dynamic=true&h=512', '720A Điện Biên Phủ, Phường 22,Quận Bình Thạnh,Hồ Chí Minh', '10.795782', '106.718823', 'https://www.360cities.net/image/landmark-tower-yokohama'),
(14, 'Bitexco', 'Tọa lạc tại trung tâm quận 1 sầm uất, tòa nhà Bitexco Financial là một tòa nhà chọc trời do tập đoàn Bitexco Group là chủ đầu tư. Từ khi được khánh thành năm 2010 đến nay, Bitexco luôn là điểm đến yêu thích của giới trẻ thành phố vì là nơi tích hợp của mua sắm, ăn uống, xem phim, vui chơi giải trí và những nhà hàng, quán bar sang trọng trên tầng thượng.', 'itexco được trang bị hệ thống thang máy 12 tầng, có tổng cộng 12 thang máy 2 tầng bên trong tòa nhà, di chuyển với tốc độ 7m/giây, tương đương du khách chỉ mất 45 giây để đi bất cứ nơi đâu trong tòa nhà.Giờ mở cửa: 10:00 – 21:30 (cuối tuần đóng cửa muộn hơn)', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/08/anh-27.png', 'Tòa nhà tài chính Bitexco, 7, 2 Hải Triều, Bến Nghé, Quận 1, Hồ Chí Minh', '10.771833', '106.704458', 'http://www.360cities.net/image/bitexco-camera-360#-17.87,0.69,70.0'),
(15, 'Hồ Con Rùa', 'Năm 1878, tháp nước được xây tại vị đây nhằm mục đích cung cấp nước cho người dân xung quanh. Đến năm 1921 thì tháp nước bị phá bỏ do không còn khả năng cung cấp nước đủ cho nhu cầu của tất cả mọi người. Từ đó vị trí này trở thành giao lộ như ngày nay.', 'Một lưu ý khi đến chơi hồ con rùa là bạn không nên để xe ngay khu vực hồ vì có thể bị công an, dân phòng cảnh cáo, thay vào đó là gửi xe ở trụ sở cơ quan đối diện hồ con rùa theo hướng Phạm Ngọc Thạch nhé. Noài ra bạn cũng có thể gửi giữ xe ở hồ con rùa bên nhà văn hoá thanh niên đó. Chi phí gửi xe ở đây có thể giao động từ 5 – 10k đồng và tăng vào các dịp lễ tết đấy!', 'https://kenh14cdn.com/thumb_w/640/d4b5d372a0/2015/08/05/avatahoconrua-8cc03.jpg', 'Giao điểm của các đường Võ Văn Tần, Phạm Ngọc Thạch, Trần Cao Vân, phường 6, Quận 3, Thành phố Hồ Chí Minh Việt Nam', '10.782900', '106.695939', 'https://www.360cities.net/image/turte-lake-hochiminh-city-vietnam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `diadiemdl`
--

CREATE TABLE `diadiemdl` (
  `id` int(11) NOT NULL,
  `text` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `Recommend` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `photo` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `latitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `longitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `panorama` text CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `diadiemdl`
--

INSERT INTO `diadiemdl` (`id`, `text`, `Description`, `Recommend`, `photo`, `address`, `latitude`, `longitude`, `panorama`) VALUES
(1, 'Hồ Xuân Hương', 'Hồ Xuân Hương là hồ đẹp nhất nằm ở trung tâm thành phố Ðà Lạt. Người ta ví Hồ Xuân Hương như trái tim của thành phố Đà Lạt. Hồ có hình mảnh trăng lưỡi liềm, là nơi thơ mộng, cuốn hút khách nhàn du, cũng là nơi hò hẹn của những đôi bạn tâm tình. Mặt hồ phẳng lặng như tấm kính pha lê, soi bóng những hàng thông reo hát suốt ngày đêm. Những con đường quanh hồ rợp bóng cây tùng, tạo thêm vẻ thơ mộng cho hồ', 'Đã đến hồ Xuân Hương mà lại không thử trải nghiệm 1 trong 5 điều sau đây lại có lỗi với bản thân lắm lắm:Đạp vịt giữa hồ cùng người ấy,Cưỡi xe ngựa đi dạo quanh hồ,Uống cafe ngắm hồ Xuân Hương,Đón bình mình trên lòng hồ,Đi dạo hồ Xuân Hương vào buổi tối', 'https://agotourist.com/wp-content/uploads/2018/09/dia-diem-du-lich-da-lat.jpg', 'Trung tâm Đà Lạt, Lâm Đồng', '11.940917', '108.441559', NULL),
(2, 'Vườn Hoa Thành Phố', 'Đã nhắc tời Đà Lạt là phải nói đến một xứ sở vô vàng loài hoa. Với tất cả các loài hoa trong lẫn ngoài nước được tập trung ở đây. Vườn Hoa  Thành Phố tọa lạc ở phía bắc của Hồ Xuân Hương. Nằm trên đồi cù  xanh mộng mơ với một địa hình  rất lý tưởng. Tên gọi trước đây của vườn hoa được gọi là Bích Câu', 'Giá vé vào cổng đối với người lớn 50.000 đồng/ lượt.Giá vé đối với trẻ em là 20.000 đồng/ lượt.Lưu ý đối với du khách là giá vé này chỉ áp dụng một chiều duy nhất. Khi ra ngoài và vô lại vẫn tính vé lại bình thường.Vườn hoa Đà Lạt chính thức đón những vị khách đầu tiên của ngày mới. Bắt đầu từ lúc 7 giờ 30 phút sáng cho đến 17 giờ hàng ngày.', 'https://agotourist.com/wp-content/uploads/2018/09/dia-diem-du-lich-o-trung-tam-da-lat-vuon-hoa-tp.jpg', 'Đà Lạt nằm quanh trên bờ phía bắc của Hồ Xuân Hương, trên thung lũng của Đồi Cù', '11.950558', '108.449728', NULL),
(3, 'Hồ Tuyền Lâm', 'Khu du lịch sinh thái hồ Tuyền Lâm là địa điểm du lịch mà bạn không thể nào bỏ qua. Khi bạn cùng gia đình đến tham quan nghỉ dưỡng tại thành phố Đà Lạt.  Hồ Tuyền Lâm là hồ nước nằm ở ngoại ô thành phố Đà Lạt. Ngoài hồ Xuân Hương là hồ nước nằm trong thành phố. Thì Hồ Tuyền Lâm được du khách biết đến rất nhiều khi đi du lịch Đà Lạt', 'Khi đến tham quan hồ Tuyền Lâm Đà Lạt du khách sẽ không mất phí vé nhé. Nên du khách đến đây cần lưu ý và tránh bị lừa nhé.', 'https://agotourist.com/wp-content/uploads/2018/09/diem-du-lich-secrec-garden-da-lat.jpg', 'Phường 04, Thành phố Đà Lạt, Tỉnh Lâm Đồng', '11.901174', '108.429573', NULL),
(4, 'Đồi Hoa Lavender', 'Vườn hoa Lavender Đà Lạt mạng một nét đẹp cực kỳ khuyến rũ. Làm lôi cuốn biết bao trái tim của những du khách khi có dịp nhìn thấy nó. Một màu tí  mơ màng mộng mị làm sao xuyến bao người. Mỗi mùa hoa lavender Đà Lạt nở là mùa mà thành phố Đà Lạt thêm nhộn nhịp hơn. Bởi vào những tháng này du khách đủ bộ lên Đà Lạt để ngắm loài hoa này rất đông. Loài hoa này đã níu giữ không biết bao trái tim của du khách thập phương khi đặt chân đến đây.', 'Khi tham quan du lịch vườn hoa Lavender nên giữ gìn vệ sinh chung. Không được xã rác bừa bãi làm mất vẻ đẹp của cánh đồng hoa.Đặc biệt du khách không được bẻ hoa và dẫm đạp lên hoa. Khi tham quan nên đi đi theo luống hoa.Vui lòng không chạy nhảy nô đùa làm ảnh hưởng người xung quanh. Tránh trường hợp làm hư hoặc gãy hoa.Nếu như du khách nào có nhu cầu mua hoa lavender về trồng. Du khách có thể liên hệ trực tiếp với chỉ vườn để được mua. Cũng như được chủ vườn hướng dẫn cách thức chăm sóc cho loài hoa này nhé.', 'https://agotourist.com/wp-content/uploads/2018/09/dia-diem-du-lich-da-lat-doi-hoa-levender.jpg', 'Cam Ly, Phường 7, Thành phố Đà Lạt, Lâm Đồng', '11.964826', '108.389575', NULL),
(5, 'Đồi Chè Cầu Đất', 'Đồi chè Cầu Đất Đà Lạt nằm cách thành phố Đà Lạt hơn 22 cây số về hướng Đông Nam. Các cánh đồng trà trải dài trên những ngọn đồi nối tiếp nhau. Nơi đây cùng với Bảo Lộc được mệnh danh là xứ sở chè của vùng Tây Nguyên, quê hương của nhiều danh trà nổi tiếng như Ô Long, Lục Trà', 'Đường đi đồi chè : Các bạn lần đầu thì nhớ hỏi kỹ đường đi trước hay dùng điện thoại thông mình để tìm đường nhé cẩn thận không lạc.Thời tiết : Các bạn nên chọn sáng sớm hoặc chiều tà vì lúc này có sương rất đẹp cho việc sống ảo.Không nên đi giữa trưa vì ở đây rất ít bóng mát và bạn sẽ bị say nắng ngay. Ngày mưa thì thôi bỏ luôn đi nha, vì đường sẽ rất bẩn, trơn trượt và quan cảnh u ám lắm.Ăn uống : khu vực này rất ít chỗ ăn uống ngon nên các đừng ăn nhé , còn nếu có ăn thì nên hỏi giá trước', 'https://agotourist.com/wp-content/uploads/2018/10/dac-san-da-lat-cau-dat-trong-cafe-moka.jpg', 'Thôn Cầu Đất – Xã Xuân Trường – Phường 12 – Tp. Đà Lạt', '11.874949', '108.559010', NULL),
(6, 'Cafe Mê Linh Garden', 'Đi du lịch Đà Lạt nếu bạn muốn tìm đến không gian thoáng đãng, hòa mình vào thiên nhiên rộng lớn thì Mê Linh Coffee Garden Đà Lạt chính là điểm đến lí tưởng. Mê Linh Coffee Garden là một quán kết hợp giữa thưởng thức cà phê và ngắm cảnh. Đây được xem như một đồn điền cỡ nhỏ ở vùng nông, tách biệt với thị thành chật chội, tù túng.', 'Dù đi Đà Lạt vào tháng mấy thì bạn cũng sẽ cảm nhận được những điều đặc biệt ở đây. Thoát ra những điều tầm thường trong cuộc sống, đến với Mê Linh Coffee Garden bạn sẽ thấy thiên đường là có thật.Cà phê chồn có vị đậm đà, không đắng như cà phê thông thường, nhưng bùi bùi, thơm nhẹ mùi xạ hương. Giá cho một ly cà phê chồn tầm 60.000đ, còn những thức uống khác dao động từ 20.000đ - 40.000đ.đến với Mê Linh Coffee Garden bạn còn được tham quan và lắng nghe quy trình trồng, chăm sóc, thu hoạch rồi chế biến từ một loại nông sản trở thành hương vị cà phê mang lại giá trị cao trên thế giới.Và điều đặc biệt cuối cùng là khi đến đây bạn vẫn có thể tha hồ ngắm những cánh đồng hoa hướng dương, cẩm tú cầu, dã quỳ dài bất tận.', 'https://agotourist.com/wp-content/uploads/2018/09/cafe-me-linh-diem-du-lich-da-lat-mien-phi.jpg', 'Tổ 20, Lâm Đồng', '11.899873', '108.347897', NULL),
(7, 'Đồng Hoa Cẩm Tú Cầu', 'Tuy chỉ là một trong những địa điểm tham quan du lịch mới nổi hiện nay. Nhưng cảnh sắc của vườn cẩm tú cầu này đã làm điêu đứng biết bao con tim giới trẻ khi lỡ đặt chân đến đây. Với nền trời xanh ngắt, không gian bao la rộng lớn. Đã tô điểm thêm cho vườn tú cầu ngày càng thanh sắc, đẹp đẽ', 'Giá vé để vào vườn hoa cẩm tú cầu ở Đà Lạt là 15.000VNĐ/người . Chỉ với 15.000 VNĐ là các bạn có thể thỏa mình ngắm nhìn được những  vườn hoa cẩm tú cầu loài hoa đang HOT nhất đối với giới trẻ  hiện nay.Nếu các bạn có nhu cầu mua hoa cẩm tú cầu về để trồng. Các bạn có thể liên hệ trực tiếp tại đây. Để mua được với một mức giá rẻ nhất nhé. Các bạn hãy yên tâm nếu khi mua hoa. Thì đơn nhiên những người chủ nơi đây sẽ chia sẽ các kinh nghiệm giúp bạn chăm hoa. Giúp bạn chăm sóc hoa nở rộ một các tốt nhất ', 'https://agotourist.com/wp-content/uploads/2018/08/chup-anh-cuoi-da-lat-tai-canh-dong-cam-tu-cau-2.jpg', 'Tổ 1, Thôn, Đường Lộc Quý, Xuân Thọ, Thành phố Đà Lạt, Lâm Đồng', '11.947052', '108.513508', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `diadiemhn`
--

CREATE TABLE `diadiemhn` (
  `id` int(11) NOT NULL,
  `text` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `Recommend` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `photo` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `latitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `longitude` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `panorama` text CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `diadiemhn`
--

INSERT INTO `diadiemhn` (`id`, `text`, `Description`, `Recommend`, `photo`, `address`, `latitude`, `longitude`, `panorama`) VALUES
(1, 'Hồ Gươm', 'Trước kia, hồ Hoàn Kiếm (hồ Gươm) còn có các tên gọi là hồ Lục Thủy (vì nước có màu xanh quanh năm), hồ Thủy Quân (dùng để duyệt thủy binh)… Đến thế kỷ XV, hồ được đổi tên thành hồ Hoàn Kiếm, gắn liền với truyền thuyết vua Lê Thái Tổ trả gươm báu cho Rùa thần.', 'Hồ Hoàn Kiếm là khu vực trung tâm của thủ đô Hà Nội, vì vậy các bạn có thể đến đây dễ dàng bằng phương tiện cá nhân hay phương tiện công cộng', 'https://www.vietfuntravel.com.vn/image/data/Ha-Noi/ho-hoan-kiem/Gioi-thieu-doi-net-ve-Ho-Hoan-Kiem-Ho-Guom-o-Ha-Noi-3.jpg', 'quận Hoàn Kiếm, Hà Nội', '21.028915', '105.852161', 'https://www.360cities.net/image/hoam-kiem-lake'),
(2, 'Quảng Trường Ba Đình', 'Quảng trường Ba Đình là quảng trường lớn nhất Việt Nam, nằm trên đường Hùng Vương, quận Ba Đình và là nơi Lăng Chủ tịch Hồ Chí Minh được xây dựng. Quảng trường này còn là nơi ghi nhận nhiều dấu ấn quan trọng trong lịch sử Việt Nam, đặc biệt, vào ngày 2 tháng 9 năm 1945, Chủ tịch Chính phủ Cách mạng lâm thời Việt Nam Dân chủ Cộng hòa Hồ Chí Minh đã đọc bản Tuyên ngôn độc lập khai sinh ra nước Việt Nam Dân chủ Cộng hòa.', 'Giá vé vào cửa của lăng Chủ tịch với du khách nước ngoài là 25.000đ/lượt, và 25.000đ vé tham quan khu nhà sàn Bác Hồ, nơi khi xưa Bác từng ở. Đối với người Việt Nam, không mất vé vào cửa', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/quang-truong-ba-dinh-54bcaffd67cd7-1-768x480.jpg', 'Hùng Vương, Điện Bàn, Ba Đình, Hà Nội', '21.037615', '105.836159', 'https://www.360cities.net/image/ba-dinh-square-1-hanoi'),
(3, 'Phố Cổ Hà Nội', 'Là quần thể di tích về trường đại học đầu tiên của nước ta, Văn Miếu không chỉ là di tích lịch sử văn hóa mà còn là nơi được rất nhiều sĩ tử, học trò tới đây để cầu may mắn trong thi cử, học hành', 'Hiện nay, khách du lịch và người dân vào tham quan Văn Miếu phải mua vé vào cổng. Giá vé dành cho người lớn là 20.000đ và vé trẻ em là 10.000đ. Đây là mức giá khá rẻ và áp dụng chung cho cả khách Việt Nam lẫn khách nước ngoài', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/pho-co-ha-noi-768x960.jpg', 'Quận Hoàn Kiếm', '21.029665', '105.836153', 'https://www.360cities.net/image/old-town-dinh-liet-eating-street-hanoi-vietnam-hoan-kiem-district'),
(4, 'Văn Miếu Quốc Tử Giám', 'Là quần thể di tích về trường đại học đầu tiên của nước ta, Văn Miếu không chỉ là di tích lịch sử văn hóa mà còn là nơi được rất nhiều sĩ tử, học trò tới đây để cầu may mắn trong thi cử, học hành', 'Hiện nay, khách du lịch và người dân vào tham quan Văn Miếu phải mua vé vào cổng. Giá vé dành cho người lớn là 20.000đ và vé trẻ em là 10.000đ. Đây là mức giá khá rẻ và áp dụng chung cho cả khách Việt Nam lẫn khách nước ngoài', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/van-mieu-quoc-tu-giam-ha-noi-768x768.jpg', '58 Quốc Tử Giám, Văn Miếu, Đống Đa, Hà Nội', '21.029665', '105.836153', 'https://www.360cities.net/image/literaturtempel-vn-miu-quc-t-gim-hanoi'),
(5, 'Nhà Thờ Lớn Hà Nội', 'Nằm tại số 40 Nhà Chung, Nhà thờ lớn Hà Nội là nhà thờ chính tòa của Tổng giáo phận Hà Nội. Với kiến trúc cổ kính, uy nghiêm, đây không chỉ là nơi hành hương của tín đồ công giáo mà còn là điểm đến du lịch nổi tiếng của thủ đô', 'Nhà thờ thường có 2 thánh lễ vào ngày thường và 7 thánh lễ vào chủ nhật. Ngày 19 tháng 3 hằng năm, nơi đây sẽ tổ chức lễ rước thánh Quan thầy của Tổng giáo phận Hà Nội,Không chỉ là địa điểm tôn giáo nổi tiếng của thủ đô, Nhà thờ lớn Hà Nội còn là điểm đến thu hút rất đông các bạn trẻ và du khách tới tham quan, chụp ảnh. Tuy nhiên bạn sẽ cần phải tới từ sớm bởi chỉ chập choạng tối là nơi đây đã đông nghẹt người.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/nha-tho-lon-ha-noi-768x959.jpg', '40 Nhà Chung, Hàng Trống, Hoàn Kiếm, Hà Nội', '21.028692', '105.848822', 'https://www.360cities.net/image/cathedral-hanoi'),
(6, 'Chùa Trấn Quốc', 'Nằm ở phía đông Hồ Tây, chùa Trấn Quốc với tuổi đời hơn 1500 năm là ngôi chùa cổ và linh thiêng bậc nhất Hà Nội. Từng là trung tâm Phật giáo của Thăng Long dưới thời Lý – Trần, chùa Trấn Quốc giờ đây trở thành điểm đến tâm linh hấp dẫn của thủ đô, thu hút rất đông du khách tới thăm quan và lễ bái mỗi năm', 'Không chỉ trong dịp Tết mà nhà chùa liên tục mở cửa đón phật tử và nhân dân đến hành lễ, chiêm bái. Đặc biệt, trong đêm giao thừa tết Nguyên đán, nhà chùa mở cửa và tổ chức khóa lễ cầu nguyện cầu quốc thái dân an, đây là khóa lễ thiêng liêng nhất trong năm do nhà chùa tổ chức.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/chua-tran-quoc-ha-noi-1-768x512.jpg', 'Đường Thanh Niên, Yên Phụ, Tây Hồ, Hà Nội', '21.048235', '105.836983', 'https://www.360cities.net/image/chua-tran-quoc-temple'),
(7, 'Hồ Tây', 'Hồ Tây trước kia có nhiều tên gọi khác như Đầm Xác Cáo, Hồ Kim Ngưu, Lãng Bạc, Dâm Đàm, Đoài Hồ, là hồ nước tự nhiên lớn nhất trong nội thành Hà Nội. Mỗi tên gọi này của hồ lại gắn liền với những sự tích khác nhau', 'Những địa điểm du lịch nổi tiếng gần Hồ Tây:Chùa Vạn Niên,Chùa Thiên Niên,Chùa Võng Thị,Đền Quán Thánh,Phủ Tây Hồ,Chùa Tảo Sách,Bến Hàn Quốc,...', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/ho-tay-ha-noi-768x501.jpg', 'Quận Tây Hồ, Hà Nội', '21.054827', '105.826568', 'https://www.360cities.net/image/west-lake-sunset-1'),
(8, 'Nhà Hát Lớn Hà Nội ', 'Đây là một công trình kiến trúc được xây dựng bởi người Pháp vào những năm 1901 – 1911 (trong khoảng thời gian đô hộ Việt Nam), lúc bấy giờ nhà hát là nơi chuyên trình diễn các tiết mục nghệ thuật cổ điển xa xỉ như Opera, nhạc thính phòng, kịch nói… cho tầng lớp quan lại hay giới thượng lưu Pháp và một số tư sản Việt. Được lấy cảm hứng từ các công trình kiến trúc Châu Âu nổi tiếng như nhà hát Opera Paris, lâu đài Tuylory… nên “hồn” Châu Âu thấm đượm nơi đây', 'Giá vé tham quan.Người lớn: 400.000 đồng/lượt.Học sinh, sinh viên: giảm 50% giá vé.Giờ mở cửa tham quan: thứ Hai và thứ Sáu hàng tuần (từ 10h30 – 12h00).Với giá vé trên, du khách sẽ được tìm hiểu kiến trúc, lịch sử hình thành Nhà hát Lớn; được xem chương trình biểu diễn nghệ thuật trong nhà hát', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/nha-hat-lon-ha-noi-1-768x768.jpg', 'Số 01 Tràng Tiền, Phan Chu Trinh, Hoàn Kiếm, Hà Nội', '21.024444', '105.857857', 'https://www.360cities.net/image/opera-house-hanoi'),
(9, 'Nhà Tù Hoả Lò', 'Nhà tù Hỏa Lò, hay còn gọi là ngục Hỏa Lò, xưa có tên tiếng Pháp là Maison Centrale, có nghĩa là đề lao trung ương, còn tên tiếng việt là Ngục thất Hà Nội, là một nhà tù cũ nằm trên phố Hỏa Lò, quận Hoàn Kiếm, Hà Nội', 'Nơi đây mở cửa cho tất cả những ai có nhu cầu, mong muốn đến tham quan với mức giá vé vô cùng dễ chịu, 30.000 VND/người, giảm 50% giá cho học sinh, sinh viên, người khuyết tật, người cao tuổi hay những ai thuộc vào diện chính sách xã hội. Ngoài ra, các đối tượng như trẻ em dưới 15 tuổi, người khuyết tật đặc biệt nặng hay người có công với Cách mạng sẽ được miễn hoàn toàn giá vé.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/dia-diem-du-lich-ha-noi-nha-tu-hoa-lo-768x512.jpg', '1 Hoả Lò, Trần Hưng Đạo, Hoàn Kiếm, Hà Nội', '21.025515', '105.846368', 'https://www.360cities.net/image/hoa-lo-prison-hanoi-testing-2'),
(10, 'Hoàn Thành Thăng Long', 'Đây là công trình kiến trúc đồ sộ, được các triều đại xây dựng trong nhiều giai đoạn lịch sử và trở thành di tích quan trọng bậc nhất trong hệ thống các di tích của Việt Nam. Hoàng thành Thăng Long đã được UNESCO công nhận là di sản văn hóa của thế giới.', 'Hoàng Thành Thăng Long mở cửa các ngày trong tuần (trừ thứ 2).Sáng: 8h00 – 11h30;Chiều : 14h00 – 17h00.Giá vé tham quan để vào khu du tích là : 30.000đ/lượt.Đối với học sinh, sinh viên từ 15 tuổi trở lên (phải có thẻ học sinh, sinh viên), người cao tuổi 60 tuổi trở lên giá vé vào cửa là : 15.000đ/lượt.Riêng đối với trẻ em dưới 15 tuổi và người có công với cách mạng hoàn toàn được miễn phí vé vào cửa.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/hoang-thanh-thang-long-768x512.jpg', '19C Hoàng Diệu, Điện Bàn, Ba Đình, Hà Nội', '21.034631', '105.840125', 'https://www.360cities.net/image/doan-mon-one-of-the-five-vestiges-of-thang-long-royal-citadel-in-hanoi'),
(11, 'Cột Cờ Hà Nội', 'Cột cờ Hà Nội còn được có cái tên khác là Kỳ đài Hà Nội, được xây dựng từ đầu thế kỷ 19 và nay nằm trong khuôn viên của Bảo tàng Lịch sử quân sự Việt Nam. Trải qua bao nhiêu năm nhưng công trình này vẫn còn nguyên vẹn và giá trị nhất trong quần thể di tích Hoàng thành Thăng Long sau cuộc kháng chiến Pháp và chống Mỹ', 'Toàn bộ Cột cờ Hà Nội cao hơn 33m, tính cả trụ treo cờ thì là 44m. Ở đây được tham quan cả khu ngoài trời và trong nhà. Ở bên trong rất rộng rãi, thoáng mát, trưng bày súng và những tượng của những người anh hùng.Các tầng đế Cột cờ có hình vuông, nhỏ dần lên trên, chồng lên nhau, xung quanh ốp gạch. Bố cục cân đối ấy đã tạo lên những đường nét thẳng, vững vàng cho Cột cờ Hà Nội. Điều đặc biệt là trong những ngày nóng nhất của Hà Nội, nhiệt độ bên trong của Cột cờ luôn mát mẻ', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/cot-co-ha-noi-2-768x768.jpg', '28A Điện Biên Phủ, Điện Bàn, Ba Đình, Hà Nội', '21.032746', '105.839772', 'https://www.360cities.net/image/the-flag-tower-of-hanoi'),
(12, 'Chùa Một Cột', 'Chùa Một Cột là một cụm kiến trúc gồm chùa và tòa đài xây dựng dưới hồ vuông. Chùa còn có tên gọi khác là Diên Hựu, nằm trong quần thể chùa Diên Hựu ở phía Tay Hoàng thành Thăng Long. Với kiến trúc độc đáo, tạo dáng như bông sen cách điệu từ dưới nước vươn lên, chùa Một Cột là một trong những biểu tượng của thủ đô Hà Nội', 'Tham quan chùa Một Cột vào bất cứ thời gian nào trong trong năm. Vào những ngày mùng 1 và 15 âm lịch đều có các lễ cúng và vào gần dịp tết nguyên đán, tham quan chùa Một Cột để chiêm ngưỡng màu hồng hoa sen nở rộ lan tỏa khắp chùa.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/chua-mot-cot-1-768x960.jpg', 'phố Chùa Một Cột, phường Đội Cấn, quận Ba Đình, thành phố Hà Nội', '21.036117', '105.833597', 'https://www.360cities.net/image/one-pillar-pagoda-in-hanoi'),
(13, 'Cầu Long Biên', 'Cầu Long Biên Hà Nội là cây cầu thép đầu tiên bắc qua con sông Hồng, nối liền hai quận Long Biên và Hoàn Kiếm.', 'Ngắm cảnh hoàng hôn lãng mạn trên cầu Long Biên cổ nhất Hà Nội,Cà phê Trần Nhật Duật ngắm cầu Long Biên,Check-in “sống ảo” ở bãi đã sông Hồng cạnh cầu Long Biên.Điểm chụp hình được ưa thích nhất ở đây chính là sân ga Long Biên. Từ bãi giữ xe ở chân cầu có thể đi bộ lên đường ray trên cầu để được thỏa sức tạo dáng cho những bức hình cực chất, nhưng nhớ chú ý tàu qua lại.Cầu có làn đường dành cho người đi bộ nhưng lan can tương đối thấp, nếu chưa quen hãy đi chậm, tránh đùa giỡn nhau sẽ rất nguy hiểm.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/cau-long-bien-1-768x960.jpg', 'Sông Hồng,Hà Nội,Việt Nam', '21.044013', '105.859731', 'https://www.360cities.net/image/long-bien-bridge-hanoi-vietnam'),
(14, 'Ga Hà Nội', 'Công trình ga Hà Nội đã gắn bó với nhiều thế hệ người Hà Nội với nhiều dấu tích thời gian. Ngay bên ngoài ga Hà Nội là một chiếc đồng hồ lớn. Thời Pháp, Hà Nội có 6 chiếc đồng hồ lớn như vậy cho người dân đạp xe đi ngang qua xem giờ nhưng nay chỉ còn lại 2 chiếc ở ga Hà Nội và Nhà thờ lớn', 'Từ trung tâm thành phố đi đến ga Hà Nội khá gần và thuận tiện. Du khách có thể đi xe bus, thuê xe ôm hay taxi đều nhanh chóng.Bước vào nhà ga, du khách sẽ được chứng kiến không khí nhộn nhịp nơi đây. Từng dòng người di chuyển qua lại, những tiếng còi tàu thỉnh thoảng vang lên rền rĩ và những nét xưa cũ còn sót lại tạo nên một bức tranh thú vị. Nhất là vào ngày Tết, du khách sẽ thấy những cành hoa đào thắm hồng nhấp nhô theo những bước chân lên tàu về quê ăn Tết.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/ga-ha-noi.jpg', 'Văn Miếu, Đống Đa, Hà Nội', '21.024321', '105.841133', 'https://www.360cities.net/image/hanoi-railway-station-at-night'),
(15, 'Chợ Đồng Xuân', 'Chợ Đồng Xuân là một trong những khu chợ đầu mối nổi tiếng ở khu vực miền Bắc. Nằm yên bình trong khu phố cổ, nhưng đây lại là một địa điểm mua sắm sầm uất bậc nhất mà chắc chắn ai cũng muốn một lần ghé thăm khi đến với Hà Nội', 'Nếu bạn chưa thông thạo đường ở đây và nhất là muốn ghé thăm chợ Đồng Xuân vào dịp cuối tuần thì nên lựa chọn phương tiện công cộng để không mất nhiều thời gian cho việc gửi xe lúc đến và tìm chỗ lấy xe lúc về. Xe điện là phương tiện thuận tiện và được du khách rất yêu thích. Xe đón khách tại điểm hẹn và đưa quý khách tham quan theo lịch trình: Hàng Đào, Hàng Ngang, Hàng Đường, Đồng Xuân , Hàng Chiếu, Nguyễn Siêu, Lò Rèn, Hàng Đồng, Hàng Vải, Hàng Buồm, Mã Mây, Hàng Bạc, Hàng Bồ, Bát Đàn, Hàng Quạt, Lương Văn Can. Lê Thái Tổ, Hàng Khay, Đinh Tiên Hoàng.', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/06/cho-dong-xuan-768x432.jpg', '15 Cầu Đông, Đồng Xuân, Hoàn Kiếm, Hà Nội', '21.038268', '105.849623', 'https://www.360cities.net/image/dong-xuan-market-1-hanoi');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `diadiemsp`
--

CREATE TABLE `diadiemsp` (
  `id` int(11) NOT NULL,
  `text` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `Recommend` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `photo` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `latitude` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `longitude` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `panorama` text CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `diadiemsp`
--

INSERT INTO `diadiemsp` (`id`, `text`, `Description`, `Recommend`, `photo`, `address`, `latitude`, `longitude`, `panorama`) VALUES
(1, 'Sunworld ', 'Sun World Fansipan Legend nằm tại phía Tây Nam thị xã Sa Pa, là quần thể công trình du lịch văn hóa, dịch vụ cáp treo, vui chơi giải trí và khách sạn nghỉ dưỡng đẳng cấp, nằm trong khung cảnh thiên nhiên hùng vĩ đã trở thành huyền thoại – đỉnh Fansipan. Đây là công trình được thực hiện với công nghệ hiện đại của hai hãng cáp treo lừng danh thế giới Doppelmayr và Garaventa, được thiết kế và thi công dựa trên những yêu cầu và kiểm duyệt khắt khe của Sun Group, đảm bảo an toàn trong mọi điều kiện.', 'Giờ vận hành cáp treo là từ 7:30 đến 17:30 hằng ngày. Bạn nên tính toán thời gian để có thể tham quan được tất cả thắng cảnh ở đây, ít nhất là nửa ngày nhé. Bạn nên chọn khoảng thời gian từ tháng 3-tháng 5 và tháng 9-tháng 10 hằng năm để kết hợp du lịch Sapa và Fansipan', 'http://divui.com/blog/wp-content/uploads/2018/10/tour-sapa-fansipan-sapa-696x398.jpg', 'TT. Sa Pa, Sa Pa, Lào Cai', '22.335173', '103.840795', NULL),
(2, 'Thung Lũng Hoa Hồng', 'Thung lũng hoa hồng nằm trong khu du lịch sinh thái ATI trên đường Mường Hoa có diện tích khoảng 22 ha, được tạo bởi hai dãy núi Hàm Rồng và Phan Xi Păng. Đây là một khu du lịch sinh thái được đầu tư suốt 3 năm, thiết kế cảnh quan sinh thái với gần triệu gốc hoa hồng, đào, mận, hồng giòn Hoa Kỳ, hạnh nhân Đài Loan…', 'Bạn nên đến Thung lũng hoa hồng Sapa vào mùa mận chín, bạn sẽ có cảm giác như đang đi du lịch Nhật Bản vào mùa hoa anh đào. Không chỉ bởi không gian lãng mạn, nên thơ mà còn là cảm giác được tận hưởng không khí se lạnh trong tiết trời mù sương.', 'http://divui.com/blog/wp-content/uploads/2018/10/du-lich-thung-lung-hoa-hong-sapa-696x497.jpg', '1 Mường Hoa, TT. Sa Pa, Sa Pa, Lào Cai', '22.333330', '103.842983', NULL),
(3, 'Bản Cát Cát', 'Cát Cát là một trong những bản làng đẹp nhất ở Sapa, diện tịch cũng thuộc vào loại nhỏ nhất. Là nơi được đầu tư và tái đầu tư phục vụ du lịch nhiều nhất tại Sapa. Đi Sapa thì không thể bỏ qua bản làng xinh xắn này được', 'Vé vào cổng bản Cát Cát mới nhất là 70K/người lớn, và 30K/trẻ em.Các điều nên làm khi đi đến đây:Chụp hình với cảnh núi và ruộng bậc thang,Dừng chân đến các ngôi nhà dân tộc thiểu số trong làng và khám phá cuộc sống của họ,Thuê đồ truyền thống để mặc chụp hình,Đi mua sắm,Uống rượu táo,Trò chuyện với dân bản.,Check in thác nước hùng vĩ. Trekking đến tận đáy thung lũng Mường Hoa và ngắm nhìn Thác Cát Cát tuyệt đẹp,Xem múa tre,Hãy thử món ăn địa phương,Dạo quanh làng và chụp hình', 'http://divui.com/blog/wp-content/uploads/2018/10/cat-cat-696x398.jpg', 'xã San Sả Hồ, huyện Sapa', '22.329713', '103.832916', NULL),
(4, 'Nhà Thờ Đá', 'Nhà thờ đá Sapa được xây dựng vào năm 1895, là một trong những công trình kiến trúc cổ được gìn giữ trọn vẹn nhất cho đến ngày hôm nay. Nhà thờ được mệnh danh là biểu tượng kì vĩ của núi rừng Tây Bắc. Trải qua nhiều lần trùng tu, nhà thờ vẫn giữ được vẹn nguyên lốii kiến trúc độc đáo, mang đậm phong cách Pháp.', 'Có những người thích đi Nhà thờ đá Sapa vào mùa xuân, có người thích đi mùa đông,… Mỗi mùa đều có một vẻ đẹp riêng, bạn nên sắp xếp một chuyến đi phù hợp với mình nhất.Sapa hầu như đều là địa hình đồi núi nên sẽ gây khó khăn cho bạn khi đi bằng xe máy. Vì vậy, để đảm bảo an toàn nhất có thể, bạn nên đến đây bằng ô tô. Khi lên khám phá nhà thờ đá Sapa, bạn có thể thuê xe máy để thuận tiện hơn.tránh mua bán các mặt hàng không rõ nguồn gốc. Bạn nên thật cảnh giác với chỗ bán hàng không uy tín. Bởi rất có thể nó được nhập lậu từ bên Trung Quốc, bán với giá rất cao. Các sản phẩm hay xảy ra tình trạng này như thuốc, thảo dược quý,… được bán xung quanh nhà thờ đá cổ.', 'http://divui.com/blog/wp-content/uploads/2016/12/diem-du-lich-mua-dong-a21-696x398.jpg', 'Sapa Town, TT. Sa Pa, Sa Pa, Lào Cai', '22.439121', '103.839704', NULL),
(5, 'Núi Hàm Rồng', 'Núi Hàm Rồng là một ngọn núi tuyệt đẹp, thuộc dãy núi Hoàng Liên Sơn hùng vỹ, nằm cách trung tâm thị trấn Sapa khoảng 3km. Ngọn núi này cũng được xem là một trong những ngọn núi nổi tiếng nhất tại Sapa. Cái tên Hàm Rồng được đặt để mô tả lại hình dáng của ngọn núi', 'Vào mỗi mùa khác nhau thì ngọn núi này lại có những vẻ đẹp khác nhau để chúng ta có thể khám phá. Tuy nhiên theo như kinh nghiệm du lịch núi Hàm Rồng được nhiều người chia sẻ lại thì các bạn không nên đến đây vào khoảng thời gian từ tháng 7 đến tháng 8 hàng năm', 'http://divui.com/blog/wp-content/uploads/2018/10/ham-rong-696x398.jpg', 'Núi Hàm Rồng nằm sát trung tâm phường Sa Pa', '22.335448', '103.847988', NULL),
(6, 'Thác Tiên Sapa', 'Vẻ đẹp của thác Tiên Sa được tạo nên từ dãy Hoàng Liên Sơn và Suối Tiên. Nguồn nước chảy từ dãy Hoàng Liên Sơn xuống tạo nên thác Tiên Sa xinh đẹp, dòng nước sẽ tiếp tục chảy theo suối Tiên ra sông Hồng.Có lẽ vì sự xuất hiện của thác Tiên Sa nên thiên nhiên xung quanh thác có rất nhiều cây cối xanh mát, tạo nên một khung cảnh thiên nhiên vô cùng xinh đẹp, lãng mạn động lòng người', 'Muốn đến đươc thác Tiên Sa, du khách phải đi qua cây cầu Si được bắc ngang qua dòng suối thơ mộng. Ngay gần thác Tiên Sa là nhà văn hóa Mông bản Cát Cát. Những cô gái và chàng trai Mông thường tới đây tụ tập múa hát trong tiếng khèn nơi núi rừng. Xung quanh nhà văn hóa Mông còn có một số gian hàng bán những đặc sản Sapa', 'http://divui.com/blog/wp-content/uploads/2018/10/thac-tien-sa-696x398.jpg', 'Cát Cát, San Sả Hồ, Sa Pa, Lào Cai', '22.328357', '103.832323', NULL),
(7, 'Bãi Đá Cổ', 'Bãi đá cổ Sapa tựa như là một nghệ thuật độc đáo của người cổ xưa khiến cho người nhìn không thể nào quên được hình ảnh, những điều bí ẩn của nó. Có lẽ đó một nét văn hóa của thuở sơ khai.', 'Bãi đá cổ Sa Pa là một trong những di sản thiên nhiên quý giá, không chỉ chuyển tải vẻ đẹp nguyên sơ của một vùng đất mà còn thu hút khách du lịch', 'http://divui.com/blog/wp-content/uploads/2018/10/kham-pha-bai-da-co-sa-pa-696x398.jpg', 'Hầu Thào, Sa Pa, Lào Cai', '22.304302', '103.898694', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `diadiemvt`
--

CREATE TABLE `diadiemvt` (
  `id` int(11) NOT NULL,
  `text` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` text DEFAULT NULL,
  `Recommend` text DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `latitude` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `longitude` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `panorama` text CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `diadiemvt`
--

INSERT INTO `diadiemvt` (`id`, `text`, `Description`, `Recommend`, `photo`, `address`, `latitude`, `longitude`, `panorama`) VALUES
(1, 'Tượng Chúa Kito', 'Đây là một trong số những bức tượng Chúa cao nhất tại Việt Nam. Sau khi hoàn công, bức tượng này cao tới 32m và chiều dài cánh tay là 18,4m. Đứng từ xa các bạn cũng có thể nhìn thấy được bức tượng Chúa Kito Vua cao sừng sững giang tay trong nắng gió. Bức tượng được đặt trên một khối bệ chạm hình Chúa cùng 12 vị tông đồ tiên khởi.', 'Tượng Chúa Vũng Tàu thường đón rất nhiều khách du lịch mỗi ngày, vì thế giờ mở cửa là từ 7h – 17h. Muốn lên đến tượng Chúa, du khách cần leo lên khoảng 1.000 bậc thang được làm bằng đá, cao 500 mét. Tầm 100 – 200 bậc sẽ có chỗ nghỉ chân và du khách nên mang theo nước để uống cho có sức.', 'https://bazantravel.com/cdn/medias/uploads/58/58564-tuong-chua-ki-to-2-700x525.jpg', '1, Bà Rịa - Vũng Tàu', '10.326833', '107.084577', NULL),
(2, 'Ngọn Hải Đăng', 'Ngọn hải đăng Vũng Tàu được xem là cổ nhất trong tổng số 79 ngọn hải đăng của Việt Nam và là biểu tượng của thành phố biển Vũng Tàu. Pháp đã xây dựng địa điểm này vào năm 1862 để báo hiệu, chỉ dẫn cho tàu thuyền qua lại.', 'Nếu có dịp đến địa điểm tham quan Vũng Tàu, du khách nên đến ngọn hải đăng vào buổi tối để cảm nhận hết vẻ đẹp của nơi này. Với ánh đèn rực sáng, ngọn hải đăng tựa như con mắt của đất liền, cần mẫn báo hiệu cho tàu thuyền qua lại trên cả vùng biển trời rộng lớn tối sẫm.', 'https://bazantravel.com/cdn/medias/uploads/56/56114-ngon-hai-dang-vung-tau-700x700.jpg', 'Nằm Trên Đỉnh Núi Nhỏ,  Tp. Vũng Tàu', '10.334410', '107.077769', NULL),
(3, 'Đồi Con Heo', 'Đồi con heo trước đây là khu vực khai thác đá ở TP. Vũng Tàu nên hầu như không có ai đi đến nơi này. Nhưng kể từ khi khu khai thác đá dừng hoạt động và bỏ hoang, một số du khách đã lên đây ngắm cảnh và chụp được cho mình những bức ảnh check – in thật tuyệt vời.', 'Nơi ngắm cảnh và chụp ảnh đẹp nhất tại đồi con heo chính là khu vực mép đồi. Đứng ở đây, du khách sẽ thấy trước mắt là đường bờ biển Bãi Sau xinh đẹp, con đường Thùy Vân và 1 góc của thành phố biển Vũng Tàu hiện đại.Nhìn ra xa phía ngoài khơi, du khách còn được thấy Hòn Bà nằm giữa biển. Vào những ngày thủy triều rút, du khách đứng tại đỉnh đồi con heo còn được ngắm nhìn con đường dưới đáy biển độc nhất vô nhị đưa du khách từ Bãi Sau ra đảo Hòn Bà.Nếu đã đặt chân đến đồi con heo thì du khách không nên bỏ qua cơ hội chụp ảnh tại một trong những địa điểm sở hữu view biển đẹp nhất tại TP. Vũng Tàu. Con đường sỏi đá dẫn lên đồi, dải phân cách trắng – đỏ bên sườn đồi hay mỏm đá độc nhất hướng ra biển', 'https://bazantravel.com/cdn/medias/uploads/58/58472-doi-con-heo-1-700x700.jpg', 'Hẻm 222 Phan Chu Trinh, Phường 2, Thành phố Vũng Tầu, Bà Rịa - Vũng Tàu', '10.327639', '107.086467', NULL),
(4, 'Mũi Nghinh Phong', 'Mũi Nghinh Phong thực chất là một mũi đất kéo dài về phía Nam của Vũng Tàu. Như cánh tay vươn dài ra biển, Nghinh Phong tạo thành hai bãi tắm, hai vịnh lớn ở hướng Tây và hướng Đông. Đó là bãi Vọng Nguyệt (hay còn gọi là Ô Quắn) và bãi Hương Phong. Xa xa ngoài khơi là bồng đảo Hòn Bà', 'Các điều nên làm khi đến đây:Ngắm ảnh và check - in,Thăm bãi tắm Vọng Nguyệt,Cắm trại và làm tiệc nướng BBQ,...Nếu đã chán ngán với những địa điểm đông đúc, ồn ào và náo nhiệt thì không còn lựa chọn nào tuyệt hơn việc đến ngay Mũi Nghinh Phong Vũng Tàu du lịch để tận hương sự yên bình và thanh khiết nơi đây.', 'https://bazantravel.com/cdn/medias/uploads/56/56062-mui-nghinh-phong-4-700x525.jpg', 'số 1, đường Hạ Long, thành phố Vũng Tàu', '10.322207', '107.083657', NULL),
(5, 'Đồi Suối Nghệ', 'Đúng như tên gọi của nơi này, điều thú vị đầu tiên của Suối Nghệ chính là những chú cừu. Nơi này có một số đàn cừu nhỏ lẻ được người dân chăn thả trên cánh đồng. Số lượng cừu ở đây khoảng vài trăm con, cả cừu lớn và cừu nhỏ. Ở nước ta, cừu không phải là động vật phổ biến, do đó không phải ai cũng có dịp được nhìn thấy cừu tận mắt và sờ vào bộ lông dày, mềm mịn của chúng', 'Phí tham quan, chụp hình ở đây không thống nhất. Có lúc 10.000đ/ người, có lúc lại 15.000đ/ người, có khi tới 50.000đ/ người. Đi theo đoàn thì phí sẽ rẻ hơn, thậm chí có đoàn nhiều trẻ con còn được miễn phí.Nơi này chỉ có vài quán nước lụp xụp, chưa có quán ăn hay nhà vệ sinh. Do đó du khách nên chuẩn bị những thứ này trước khi đến.Dịp cuối tuần và lễ Tết, đồng cừu Suối Nghệ rất đông khách du lịch. Do đó du khách muốn được thoải mái với những chú cừu và tránh đông người thì nên đi vào ngày thường.Một số bé cừu rất háu ăn và đôi khi ăn cả bao nilon gói đậu phộng. Vì thế du khách lưu ý khi cho cừu ăn nên chú ý không vứt bao xuống đất để tránh cừu ăn phải bao nilon.', 'https://bazantravel.com/cdn/medias/uploads/56/56291-doi-cuu-vung-tau-10-700x466.jpg', 'xã Suối Nghệ, huyện Châu Đức, Bà Rịa Vũng Tàu', '10.596230', '107.195460', NULL),
(6, 'Hồ Đá Xanh', 'Hồ đá xanh (Green Stone Lake) là một địa điểm khá hấp dẫn đối với du khách, đặc biệt là những bạn trẻ yêu thích vẻ đẹp thơ mộng và trữ tình, muốn lưu lại những tấm hình kỷ niệm đẹp mắt bên bạn bè và người thân. Gọi là Hồ Đá Xanh vì hồ vốn được tạo nên bởi những đợt khai thác đá, qua thời gian nước trong hồ đầy dần và có màu xanh ngọc bích đặc trưng nên người ta gọi đó là Hồ Đá Xanh. Hồ Đá Xanh thuộc huyện Tân Thành tỉnh Bà Rịa Vũng Tàu, nằm ôm mình bên sườn núi Dinh hùng vĩ. Hồ sở hữu một làn nước xanh trong và khung cảnh hữu tình tựa như bức tranh thủy mặc', 'Mở cửa từ 8h-18h, Giá vé :Chụp hình: 40.000 đồng,Chụp ảnh cưới: 150.000 đồng,Chụp ảnh với cừu: 50.000 đồng', 'https://bazantravel.com/cdn/medias/uploads/56/56101-ho-da-xanh-vung-tau-10-700x875.jpg', 'Tân Thành, Bà Rịa - Vũng Tàu', '10.508713', '107.147341', NULL),
(7, 'Biển Hồ Cốc', 'Khu vực này có rất nhiều bãi tắm đẹp, hoang sơ, nước trong có thể nhìn thấy cả đáy. Đây là điểm du lịch lý tưởng cho kỳ nghỉ ngắn ngày đối với du khách đến từ Tp Hồ Chí Minh', 'Mùa nắng thường từ tháng 11 – tháng 4 năm sau, và mùa mưa từ tháng 5 đến tháng 10. Nhiệt độ tại Hồ Cốc cũng không quá nóng hay lạnh nên muốn đến đây khi nào cũng được cả. Chỉ cần xem dự báo trước để tránh mưa gió là có thể oanh tạc khắp chốn bồng lai này rồi', 'https://bazantravel.com/cdn/medias/uploads/57/57437-ho-coc-vung-tau-2-700x465.jpg', 'Tỉnh lộ 44A, Thị trấn Phước Hải, Huyện Đất Đỏ, Tỉnh BRVT', '10.499530', '107.477252', NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `diadiem`
--
ALTER TABLE `diadiem`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `diadiemdl`
--
ALTER TABLE `diadiemdl`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `diadiemhn`
--
ALTER TABLE `diadiemhn`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `diadiemsp`
--
ALTER TABLE `diadiemsp`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `diadiemvt`
--
ALTER TABLE `diadiemvt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `diadiem`
--
ALTER TABLE `diadiem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `diadiemdl`
--
ALTER TABLE `diadiemdl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `diadiemhn`
--
ALTER TABLE `diadiemhn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `diadiemsp`
--
ALTER TABLE `diadiemsp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `diadiemvt`
--
ALTER TABLE `diadiemvt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
